﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIItem : MonoBehaviour
{
    [SerializeField] protected UIValue itemAmount;
    [SerializeField] protected UIValue reloadAmount;

    private void OnEnable()
    {
        ActivateReloadAmount(false);
    }

    public void SetItemAmountValue(float _value)
    {
        if (itemAmount)
            itemAmount.SetCurValue(_value);
    }

    public void SetItemAmountMinMaxValue(float _min, float _max)
    {
        if (itemAmount)
            itemAmount.SetMinMaxValue(_min, _max);
    }

    public void SetReloadAmountValue(float _value)
    {
        if (reloadAmount)
            reloadAmount.SetCurValue(_value);
    }

    public void ActivateReloadAmount(bool _activate)
    {
        if (reloadAmount)
            reloadAmount.gameObject.SetActive(_activate);
    }
}

