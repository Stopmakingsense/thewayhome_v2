﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIPlayer : UIUnit
{
    [SerializeField] protected UIValue lives;
    [SerializeField] protected UIValue points;
    [SerializeField] protected GameObject pauseMenu;
    [SerializeField] protected QuickMenuUI quickMenu;
    public QuickMenuUI QuickMenu { get { return quickMenu; } }

    public void SetLivesValue(float _value)
    {
        lives.SetCurValue(_value);
    }

    public void SetPointsValue(float _value)
    {
        points.SetCurValue(_value);
    }

    public void PauseMenuSetActive(bool _active)
    {
        pauseMenu.SetActive(_active);
    }
}
