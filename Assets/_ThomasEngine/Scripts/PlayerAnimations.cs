﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimations : MonoBehaviour 
{

    //animator
    [SerializeField] private Animator anim;
    //force idle stuff
    [SerializeField] private bool forceIdle;
    [SerializeField] private string animIdleStateName = "Idle";
    //movement
    [SerializeField] private string animFacingRight = "facingRight";
    [SerializeField] private string animInputHor = "horizontal";
    [SerializeField] private string animInputVer = "vertical";
    [SerializeField] private string animRun = "run";
    [SerializeField] private string animBackwards = "backwards";
    [SerializeField] private string animCrouch = "crouch";
    [SerializeField] private string animClimbing = "climbing";
    [SerializeField] private string animGrounded = "grounded";

    //jumping
    [SerializeField] private string animJump = "jump";
    [SerializeField] private string animDoubleJump = "doubleJump";
    [SerializeField] private string animWallHitLeft = "wallHitLeft";
    [SerializeField] private string animWallHitRight = "wallHitRight";

    //health
    [SerializeField] private string animHurt = "hurt";
    [SerializeField] private string animDead = "dead";

    public bool facingRight;
    public bool jump;
    public BoolWrapper doubleJumpSwitch = new BoolWrapper(false);
    public bool grounded;
    public bool running;
    public bool backwards;
    public bool climbing;
    public bool right;
    public BoolWrapper hurt = new BoolWrapper(false);
    private bool dead;
    public bool crouching;
    public bool wallHitLeft;
    public bool wallHitRight;

    public float inputHor;
    public float inputVer;

    [SerializeField]
    private int maxDeathAnims = 1;
    [SerializeField]
    private int maxHurtAnims = 1;

    public void SetAnimationController(Animator _controller)
    {
        anim = _controller;
    }
	
	// Update is called once per frame
	void Update () 
	{
        SyncAnimations();
    }

    void SyncAnimations()
    {
        if (!anim)
        {
            anim = GetComponentInChildren<Animator>();
            if (!anim)
                return;
        }

        if (forceIdle)
        {
            AnimatorStateInfo info = anim.GetCurrentAnimatorStateInfo(0);
            if (!info.IsName(animIdleStateName) || !anim.GetBool(animGrounded))
            {
                anim.Play(animIdleStateName);
                anim.SetBool(animGrounded, true);
            }
            return;
        }

        //movement
        anim.SetBool(animFacingRight, facingRight);
        anim.SetFloat(animInputHor, Mathf.Abs(inputHor));
        anim.SetFloat(animInputVer, (inputVer));
        anim.SetBool(animGrounded, grounded);
        anim.SetBool(animRun, running);
        anim.SetBool(animBackwards, backwards);
        anim.SetBool(animCrouch, crouching);
        anim.SetBool(animClimbing, climbing);

        //jumping
        anim.SetBool(animJump, jump);
        anim.SetBool(animDoubleJump, doubleJumpSwitch.Value);
        anim.SetBool(animWallHitLeft, wallHitLeft);
        anim.SetBool(animWallHitRight, wallHitRight);

        //health
        anim.SetBool(animHurt, hurt.Value);
        anim.SetBool(animDead, dead);
    }

    
    public void SetMeleeNum(int _ind)
    {
        anim.SetInteger("meleeNum", _ind);
    }

    public void SetDead(bool _isDead)
    {
        if (_isDead)
        {
            anim.SetInteger("deathNum", Random.Range(0, maxDeathAnims));
            dead = true;
        }
        else
        {
            dead = false;
            anim.Play("Idle", 0);
        }
    }

    public void PlayHurt()
    {
        anim.SetInteger("hurtNum", Random.Range(0, maxHurtAnims));
        StartCoroutine(Utils.StartBoolTimer(hurt, 0, 1));
    }

    public void SetForceIdle(bool _forceIdle)
    {
        forceIdle = _forceIdle;
    }
}
