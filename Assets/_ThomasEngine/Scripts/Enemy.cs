﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(EnemyController))]
[RequireComponent(typeof(Collider2D))]
public class Enemy : Unit 
{

    public enum DeathMovement { StopIfGrounded, FallThrough }

    [SerializeField]
    private bool disableMovementIfStunned;
    [SerializeField]
    private float stunTime = 1;
    private bool stunned;
    public bool IsStunned() { return stunned; }
    [SerializeField]
    private float deathTime = 1;
    [SerializeField]
    private GameObject deathFX;

    [SerializeField]
    private DeathMovement deathMovement;
    [SerializeField]
    private Behaviour[] disableOnDeath;

    private EnemySoundFX soundFX;

    private EnemyController con;
    private Collider2D col;


    // Use this for initialization
    public override void Awake () 
	{
        base.Awake();

        //getcomp
        soundFX = GetComponent<EnemySoundFX>();
        col = GetComponent<Collider2D>();
        con = GetComponent<EnemyController>();
	}

    public override void AddHp(float _amount)
    {
        base.AddHp(_amount);
    }

    public override void DamageHp(float _damage)
    {
        if (dead)
            return;

        base.DamageHp(_damage);

        //kill player if health at 0
        if (curHp.Value <= 0)
        {
            curHp.Value = 0;
            StartCoroutine(StartDeath());
        }

        //play sound
        if (soundFX)
            soundFX.PlayHurtSound();

        //stun
        if (disableMovementIfStunned)
            StartCoroutine(StartStun());
    }

    IEnumerator StartDeath()
    {
        dead = true;

        DisableComponents();

        //play sound
        if (soundFX)
            soundFX.PlayDeathSound();

        Vector2 lastPos = transform.position;
        float timer = 0;
        while (timer < deathTime)
        {
            timer += Time.deltaTime;
            if (timer > deathTime)
                timer = deathTime;
            if (deathMovement == DeathMovement.StopIfGrounded)
            {
                if (con.IsGrounded)
                    transform.position = lastPos;
                else
                    lastPos = transform.position;
            }
            yield return new WaitForFixedUpdate();
        }
    
        DestroyEnemy();
    }

    IEnumerator StartStun()
    {
        stunned = true;
        //anim sync
        anim.PlayStunned();
        float timer = 0;
        while (timer < stunTime)
        {
            timer += Time.deltaTime;
            if (timer > stunTime)
                timer = stunTime;
            yield return new WaitForEndOfFrame();
        }
        stunned = false;
        //anim sync
        anim.PlayIdle();
    }

    void DisableComponents()
    {
        //always disable collider
        col.enabled = false;

        //optional disable other components
        foreach (var deathComp in disableOnDeath)
        {
            if (deathComp)
                deathComp.enabled = false;
        }
    }

    void DoDeathFX()
    {
        if (deathFX)
            Instantiate(deathFX, transform.position, Quaternion.LookRotation(Vector3.up));
    }

    void DestroyEnemy()
    {
        DoDeathFX();
        Destroy(this.gameObject);
    }

}
