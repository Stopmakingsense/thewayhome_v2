﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCamera : MonoBehaviour
{

    
    [SerializeField]
    private Vector3 offset = new Vector3(0,1,-15);
    [SerializeField]
    private float XYSensitivity = 10;

    
    private Vector3 curOffset;
    public Vector3 GetCurOffset { get { return curOffset; } }
    private bool settingPosition;
    private bool offsettingCamera;

    private Transform player;
    private Player pl;

    private void Start()
    {
        StartCoroutine(StartFindPlayerAndSetPosition());
    }

    void FixedUpdate()
    {
        if (!player)
            return;

        FollowPlayer();
    }

    void InitializeCamera()
    {
        curOffset = offset;
        transform.position = player.position + curOffset;
    }

    void FollowPlayer()
    {
        if (settingPosition || pl.IsDead)
            return;

        transform.position = Vector3.Lerp(transform.position, player.position + curOffset, Time.deltaTime * XYSensitivity);
    }

    public void OffsetCamera(Vector3 _offset, float _time, bool _smooth)
    {
        StopAllCoroutines();
        StartCoroutine(StartOffset(_offset, _time, _smooth));
    }

    public void SetCameraPosition(Vector3 _position, float _time, bool _smooth)
    {
        StopAllCoroutines();
        StartCoroutine(StartSetCamPosition(_position, _time, _smooth));
    }

    public void ResetCamera()
    {
        offsettingCamera = false;
        settingPosition = false;
        StopAllCoroutines();
        curOffset = offset;
    }

    public void ResetCamera(float _time, bool _smooth)
    {
        offsettingCamera = false;
        settingPosition = false;
        StopAllCoroutines();
        StartCoroutine(StartOffset(offset, _time, _smooth));
    }

    IEnumerator StartFindPlayerAndSetPosition()
    {
        settingPosition = true;
        while (!player)
        {

            GameObject playerGO = GameObject.FindGameObjectWithTag("Player");
            if (playerGO)
            {
                player = playerGO.transform;
                pl = player.GetComponent<Player>();
            }    
            yield return new WaitForEndOfFrame();

        }
        settingPosition = false;
        InitializeCamera();
    }

    IEnumerator StartOffset(Vector3 _offset, float _time, bool _smooth)
    {
        offsettingCamera = true;
        Vector3 curDistance = transform.position - player.position;
        float timer = 0;
        float perc = 0;
        while (perc < 1 && offsettingCamera)
        {
            timer += Time.fixedDeltaTime;
            if (timer > _time)
            {
                timer = _time;
            }
            perc = timer / _time;

            if (_smooth)
                perc = perc * perc * (3 - 2 * perc);

            curOffset = Vector3.Lerp(curDistance, _offset, perc);
            yield return new WaitForFixedUpdate();
        }
        offsettingCamera = false;
    }

    IEnumerator StartSetCamPosition(Vector3 _newPos, float _time, bool _smooth)
    {
        settingPosition = true;
        Vector3 curPos = transform.position;
        float timer = 0;
        float perc = 0;
        while (perc < 1 && settingPosition)
        {
            timer += Time.fixedDeltaTime;
            if (timer > _time)
            {
                timer = _time;
            }
            perc = timer / _time;
            if (_smooth)
                perc = perc * perc * (3f - 2f * perc);

            transform.position = Vector3.Lerp(curPos, _newPos, perc);
            yield return new WaitForFixedUpdate();
        }
    }

}
