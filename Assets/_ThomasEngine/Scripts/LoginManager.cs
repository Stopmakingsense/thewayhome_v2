﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class LoginManager : MonoBehaviour
{
    [SerializeField] private GameObject loginPanelPrefab;
    [SerializeField] private Transform loginPanelParent;
    [SerializeField] private int saveSlots = 3;
    [SerializeField] private string nextSceneToPlay;

    private UserDataManager dataManager;

    private List<LoginPanel> loadedPanels = new List<LoginPanel>();

    // Use this for initialization
    void Start()
    {
        dataManager = GameManager.instance.GetUserDataManager();
        RefreshPlayerList();
    }

    public void RefreshPlayerList()
    {
        foreach (var panel in loadedPanels)
        {
            Destroy(panel.gameObject);
        }
        loadedPanels.Clear();

        for (int i = 0; i < saveSlots; i++)
        {
            var obj = Instantiate(loginPanelPrefab, loginPanelParent);
            LoginPanel panel = obj.GetComponent<LoginPanel>();
            if (panel)
            {
                panel.SetLoginManager(this);
                panel.SetSlotInd(i);
                panel.LoadNewPlayerWindow(true);

                loadedPanels.Add(panel);
            }
        }

        DisplayPlayers();
    }

    void DisplayPlayers()
    {
            
        if (dataManager.users.Count > 0)
        {
            foreach (var panel in loadedPanels)
            {
                foreach (var user in dataManager.users)
                {
                    if (user.saveSlotId == panel.GetSlotInd())
                    {
                        panel.LoadNewPlayerWindow(false);
                        panel.SetTextData(user.playerName, user.points,
                            user.lives, user.levelUnlocked);
                    }
                }
            }

        }
        else
        {
            LoadAllNewPlayerWindows();
        }
    }

    void LoadAllNewPlayerWindows()
    {
        foreach (var panel in loadedPanels)
        {

                panel.LoadNewPlayerWindow(true);
            
        }
    }

    public string GetNextSceneToPlay()
    {
        return nextSceneToPlay;
    }

    public GameObject GetLoginPanelParent()
    {
        return loginPanelParent.gameObject;
    }

}
