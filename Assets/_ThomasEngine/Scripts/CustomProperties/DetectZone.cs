﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

//This is a custom serilizable detect zone property using Overlap functions.
//There is a custom editor property drawer for this class
[System.Serializable]
public class DetectZone
{
    public string zoneName;
    public enum PositionType { None, Local, World }
    public enum DetectAreaType { Circle, Box }
    public LayerMask detectMask;
    public DetectAreaType detectType;
    public PositionType positionType;
    public Transform trans;
    public Vector2 worldPos;
    public Vector2 offset;
    public Vector2 size = Vector2.one;
    public bool useTransformZAngle;
    public float angle = 0;
    public float radius = 1;
    public Color debugColor = Color.cyan;

    //gui stuff
    public Vector2 handlePoint;

    public Collider2D[] DetectCollidersNonAlloc(Transform _trans = null, int _maxAmount = 1)
    {
        if (_trans)
            trans = _trans;
        if (trans)
            worldPos = trans.position;
        Collider2D[] cols = new Collider2D[_maxAmount];
        var pos = worldPos + offset;
        if (useTransformZAngle && trans)
            angle = trans.eulerAngles.z;
        if (detectType == DetectAreaType.Circle)
            Physics2D.OverlapCircleNonAlloc(pos, radius, cols, detectMask);
        else if (detectType == DetectAreaType.Box)
            Physics2D.OverlapBoxNonAlloc(pos, size, angle, cols, detectMask);

        return cols;
    }

    public Collider2D[] DetectColliders(Transform _trans = null)
    {
        if (_trans)
            trans = _trans;
        if (trans)
            worldPos = trans.position;
        Collider2D[] cols = null;
        var pos = worldPos + offset;
        if (useTransformZAngle && trans)
            angle = trans.eulerAngles.z;
        if (detectType == DetectAreaType.Circle)
            cols = Physics2D.OverlapCircleAll(pos, radius, detectMask);
        else if (detectType == DetectAreaType.Box)
            cols = Physics2D.OverlapBoxAll(pos, size, angle, detectMask);

        return cols;
    }

    public Collider2D DetectCollider(Transform _trans = null)
    {
        if (_trans)
            trans = _trans;
        if (trans)
            worldPos = trans.position;

        return DetectCollider(worldPos);
    }

    public Collider2D DetectCollider(Vector2 _pos)
    {
        worldPos = _pos;
        Collider2D col = null;
        var pos = worldPos + offset;
        if (detectType == DetectAreaType.Circle)
            col = Physics2D.OverlapCircle(pos, radius, detectMask);
        else if (detectType == DetectAreaType.Box)
            col = Physics2D.OverlapBox(pos, size, angle, detectMask);

        return col;
    }

    public bool Detected(Transform _trans = null)
    {
        return DetectCollider(_trans);
    }

#if UNITY_EDITOR
    public void DrawDetectZone(Object _source, SerializedObject _sourceRef)
    {
        Handles.color = debugColor;
        EditorGUI.BeginChangeCheck();

        handlePoint = Handles.PositionHandle(worldPos, Quaternion.identity);

        //position points after dragging
        if (EditorGUI.EndChangeCheck())
        {
            Undo.RecordObject(_source, "Modified " + _source + " properties.");
            worldPos = handlePoint;
            _sourceRef.ApplyModifiedProperties();
        }

        //get final position
        var pos = worldPos + offset;

        //draw the objects
        if (detectType == DetectAreaType.Box)
            Handles.DrawWireCube(pos, size);
        else if (detectType == DetectAreaType.Circle)
            Handles.DrawWireDisc(pos, Vector3.back, radius);
    }
#endif
}


