﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public class Utils
{

    public static Transform FindDeepChild(Transform _parent, string _childName)
    {
        Transform[] children = _parent.GetComponentsInChildren<Transform>();
        foreach (var child in children)
        {
            if (child.name == _childName)
                return child;
        }
        Debug.Log("No match for name: " + _childName + " inside " + _parent.name);
        return null;  
    }

    public static string[] GetDeepChildNames(Transform _parent)
    {
        var children = _parent.GetComponentsInChildren<Transform>();
        var names = new string[children.Length];
        for (int i = 0; i < children.Length; i++)
        {
            names[i] = children[i].name;
        }
        return names;
    }

    public static Material[] GetDeepChildMaterials(Transform _parent)
    {
        var rends = _parent.GetComponentsInChildren<Renderer>();
        var startMats = new Material[rends.Length];
        for (int i = 0; i < rends.Length; i++)
        {
            startMats[i] = rends[i].material;
        }
        return startMats;
    }

    public static void SetAllChildMaterials(Transform _parent, Material _mat)
    {
        var rends = _parent.GetComponentsInChildren<Renderer>();
        for (int i = 0; i < rends.Length; i++)
        {
            rends[i].material = _mat;
        }
    }

    public static bool IsClassOrSubClass(Type _type, Type _baseClass)
    {
        return _type.IsSubclassOf(_baseClass) || _type.IsAssignableFrom(_baseClass);
    }

    public static IEnumerator StartBoolTimer(BoolWrapper _bool, float _time = default(float), int _frames = default(int) )
    {
        _bool.Value = true;
        if (_time > 0)
            yield return new WaitForSeconds(_time);
        else if (_frames > 0)
        {
            int count = 0;
            while (count < _frames)
            {
                count++;
                yield return new WaitForEndOfFrame();
            }
        }
        _bool.Value = false;
    }

    public static void LookAt2D(Transform _transform, Vector2 _lookPos, bool _consistentUpDirection = false)
    {
        var dir = _lookPos - (Vector2)_transform.position;
        _transform.right = dir;
        if (_consistentUpDirection && Vector2.Dot(_transform.up, Vector2.down) > 0)
            _transform.Rotate(180, 0, 0);
    }

    public static IEnumerator ChangeFloatValueBySpeed(FloatWrapper _curValue, float _targetValue, float _speed)
    {
        var startValue = _curValue.Value;
        var diff = Mathf.Abs(_targetValue - _curValue.Value);
        var time = diff / _speed;
        float timer = 0;
        float perc = 0;
        while (timer < time)
        {
            timer += Time.deltaTime;
            if (timer > time)
                timer = time;
            perc = timer / time;
            _curValue.Value = Mathf.Lerp(startValue, _targetValue, perc);
            yield return new WaitForEndOfFrame();

        }
    }

    public static Transform FindClosestByTag(Transform _pos, string _tag)
    {
        var objs = GameObject.FindGameObjectsWithTag(_tag);
        if (!(objs.Length > 0))
            return null;
        Transform closest = null;
        float distance = Mathf.Infinity;
        for (int i = 0; i < objs.Length; i++)
        {
            var dist = Vector2.Distance(_pos.position, objs[i].transform.position);
            if (dist < distance)
            {
                distance = dist;
                closest = objs[i].transform;
            }
                
        }
        return closest;
    }

    public static void LogComponentNullError(System.Type _type, GameObject _obj)
    {
        Debug.LogError("No " + _type.Name + " component found on " + _obj);
    }
}
