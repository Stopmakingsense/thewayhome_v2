﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ChangeSkin", menuName = "Data/Interacts/ChangeSkin", order = 1)]
public class InteractFXChangeSkin : InteractFX
{
    [SerializeField] private bool needPrerequisiteSkin;
    [SerializeField] private PlayerSkinData prerequisiteSkin;
    [SerializeField] private float delay;
    [SerializeField] private PlayerSkinData skinData;
    [SerializeField] private bool freezeReciever;
    [SerializeField] private bool doAnimations;
    [SerializeField] private float crossFadeTime;
    [SerializeField] private string senderAnimToPlay;
    [SerializeField] private string receiverAnimToPlay;
    [SerializeField] private bool spawnObjectAfterChange;
    [SerializeField] private GameObject[] spawnObjects;
    [SerializeField] private bool destroySenderAfterChange;
    [SerializeField] private bool destroyRoot;
    [SerializeField] private float destroyDelay;

    private Transform receiver;
    private Player player;
    private UnitEquip equip;
    private UnitController cont;

    public override void DoFX(GameObject _sender = null, GameObject _receiver = null)
    {
        receiver = _receiver.transform;
        player = _receiver.GetComponent<Player>();
        equip = player.GetComponent<UnitEquip>();
        cont = player.GetComponent<UnitController>();
        if (needPrerequisiteSkin)
        {
            if ((PlayerSkinData)player.curUnitData != prerequisiteSkin)
            {
                Debug.Log("Invalid skin prerequisite!");
                return;
            }

        }
        player.StartCoroutine(StartChangeSkin(_sender, _receiver));
    }

    IEnumerator StartChangeSkin(GameObject _sender = null, GameObject _receiver = null)
    {
        if (cont)
            cont.DisableAiming(true);
        if (doAnimations)
        {
            if (_sender)
            {
                var animSend = _sender.GetComponentInChildren<Animator>();
                if (animSend)
                    animSend.CrossFade(senderAnimToPlay, crossFadeTime);
            }
            if (_receiver)
            {
                var animRec = _receiver.GetComponentInChildren<Animator>();
                if (animRec)
                    animRec.CrossFade(receiverAnimToPlay, crossFadeTime);
            }

        }
        //make sure receiver lines up with sender
        float timer = 0;
        var pos = player.transform.position;
        var rot = player.transform.rotation;
        while (timer < crossFadeTime && _sender)
        {
            timer += Time.deltaTime;
            var perc = timer / crossFadeTime;
            player.transform.position = Vector2.Lerp(pos, _sender.transform.position, perc);
            player.transform.rotation = Quaternion.Lerp(rot, _sender.transform.rotation, perc);
            yield return new WaitForFixedUpdate();
        }

        //freeze player position
        timer = 0;
        while (timer < delay)
        {
            timer += Time.deltaTime;
            var perc = timer / crossFadeTime;
            if (freezeReciever)
                player.transform.position = pos;
            yield return new WaitForFixedUpdate();
        }
        if (_sender)
        {
            player.transform.rotation = _sender.transform.rotation;
            player.transform.position = _sender.transform.position;
        }
        //do swap
        player.SetPlayerSkin(skinData);
        if (equip)
            equip.GetSpawnLocation();
        SpawnObjects();
        DestroySender(_sender);

        if (cont)
            cont.DisableAiming(false);
    }

    void SpawnObjects()
    {
        if (!spawnObjectAfterChange)
            return;

            foreach (var spawn in spawnObjects)
            {
                Instantiate(spawn, receiver.position, receiver.rotation);
            }
    }

    void DestroySender(GameObject _obj)
    {
        if (!destroySenderAfterChange)
            return;

        var objToDestroy = _obj;
        if (destroyRoot)
            objToDestroy = _obj.transform.root.gameObject;

        Destroy(objToDestroy, delay);
    }
}
