﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "AddForce", menuName = "Data/Interacts/AddForce", order = 1)]
public class InteractFXAddForce : InteractFX
{
    public enum DirectionType { ClosestPointAngle, XOnly, Override }
    [SerializeField] private DirectionType directionType;
    [SerializeField] private Vector2 direction;

    [SerializeField] private float force = 1;

    public override void DoFX(GameObject _sender, GameObject _receiver)
    {
        UnitController cont = _receiver.GetComponent<UnitController>();
        Collider2D col = _receiver.GetComponent<Collider2D>();
        Vector2 center = _sender.transform.position;

        //gather direction information
        Vector2 dir = direction;
        if (directionType == DirectionType.ClosestPointAngle)
            dir = ((Vector2)col.bounds.ClosestPoint(center) - center).normalized;
        else if (directionType == DirectionType.XOnly)
        {
            dir = (Vector2)col.transform.position - center;
            if (dir.x > 0)
                dir = new Vector2(1, 0);
            else if (dir.x < 0)
                dir = new Vector2(-1, 0);
        }

        cont.Bounce(dir, force);
    }
}
