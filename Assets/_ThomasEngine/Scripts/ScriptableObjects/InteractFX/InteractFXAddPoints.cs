﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "AddPoints", menuName = "Data/Interacts/AddPoints", order = 1)]
public class InteractFXAddPoints : InteractFX
{

    [SerializeField] private int pointsToAdd = 1;

    public override void DoFX(GameObject _sender, GameObject _receiver)
    {
        GameManager.instance.SpawnedPlayer.GetComponent<Player>().AddPoints(pointsToAdd);
    }
}
