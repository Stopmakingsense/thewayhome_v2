﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "AimFX", menuName = "Data/FX/AimFX", order = 1)]
public class AimFX : ScriptableObject
{
    public enum LineType { Constant, Firefade }
    public GameObject aimRetical;
    public GameObject lineRenderer;
    public LineType lineType;
    public float fadeTime;
}
