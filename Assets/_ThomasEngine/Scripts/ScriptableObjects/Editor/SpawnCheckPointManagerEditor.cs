﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Reflection;

[CustomEditor(typeof(SpawnCheckPointManager))]
public class SpawnCheckPointManagerEditor : Editor
{

    private SpawnCheckPointManager source;
    private SerializedObject sourceRef;

    private SerializedProperty overrideCheckPoint;
    private SerializedProperty checkPoint;
    private SerializedProperty checkPoints;
    //player stuff
    private SerializedProperty playerSpawn;
    private SerializedProperty respawnTime;
    //saving stuff
    private SerializedProperty resetLevelOnDeath;
    private SerializedProperty progressOnly;
    private SerializedProperty saveProgressToDisc;
    private SerializedProperty resetProgressOnQuit;
    //level finish
    private SerializedProperty sceneUnlocked;
    private SerializedProperty nextSceneToPlay;
    private SerializedProperty freezeGame;
    private SerializedProperty freezePlayer;
    private SerializedProperty endTime;

    private DetectZone[] points;

    private void OnEnable()
    {
        source = (SpawnCheckPointManager)target;
        sourceRef = serializedObject;

        SceneView.onSceneGUIDelegate += OnSceneGUI;

        GetProperties();
    }

    private void OnDisable()
    {
        SceneView.onSceneGUIDelegate -= OnSceneGUI;
    }

    public override void OnInspectorGUI()
    {
        SetProperties();
        SetCheckPointNames();
        
        sourceRef.ApplyModifiedProperties();
    }

    void GetProperties()
    {
        overrideCheckPoint = sourceRef.FindProperty("overrideCheckPoint");
        checkPoint = sourceRef.FindProperty("checkPoint");
        checkPoints = sourceRef.FindProperty("checkPoints");

        //playerstuff
        playerSpawn = sourceRef.FindProperty("playerSpawn");
        respawnTime = sourceRef.FindProperty("respawnTime");

        //saving
        resetLevelOnDeath = sourceRef.FindProperty("resetLevelOnDeath");
        progressOnly = sourceRef.FindProperty("progressOnly");
        saveProgressToDisc = sourceRef.FindProperty("saveProgressToDisc");
        resetProgressOnQuit = sourceRef.FindProperty("resetProgressOnQuit");

        //level finish
        sceneUnlocked = sourceRef.FindProperty("sceneUnlocked");
        nextSceneToPlay = sourceRef.FindProperty("nextSceneToPlay");
        freezeGame = sourceRef.FindProperty("freezeGame");
        freezePlayer = sourceRef.FindProperty("freezePlayer");
        endTime = sourceRef.FindProperty("endTime");
    }

    void SetProperties()
    {
        SetCheckPointLength();
        EditorGUILayout.LabelField("------------------------");
        EditorGUILayout.LabelField("CheckPoint Options");
        EditorGUILayout.LabelField("------------------------");
        EditorGUILayout.PropertyField(overrideCheckPoint);
        if (overrideCheckPoint.boolValue)
            EditorGUILayout.PropertyField(checkPoint);
        EditorGUILayout.PropertyField(checkPoints, true);
        //playerstuff
        EditorGUILayout.LabelField("------------------------");
        EditorGUILayout.LabelField("Player Options");
        EditorGUILayout.LabelField("------------------------");
        EditorGUILayout.PropertyField(playerSpawn);
        EditorGUILayout.PropertyField(respawnTime);
        //saving
        EditorGUILayout.LabelField("------------------------");
        EditorGUILayout.LabelField("Data Options");
        EditorGUILayout.LabelField("------------------------");
        EditorGUILayout.PropertyField(resetLevelOnDeath);
        EditorGUILayout.PropertyField(progressOnly);
        EditorGUILayout.PropertyField(saveProgressToDisc);
        EditorGUILayout.PropertyField(resetProgressOnQuit);
        //level finish
        EditorGUILayout.LabelField("------------------------");
        EditorGUILayout.LabelField("On Level Finish");
        EditorGUILayout.LabelField("------------------------");
        EditorGUILayout.PropertyField(sceneUnlocked);
        EditorGUILayout.PropertyField(nextSceneToPlay);
        EditorGUILayout.PropertyField(freezeGame);
        EditorGUILayout.PropertyField(freezePlayer);
        EditorGUILayout.PropertyField(endTime);

    }

    void SetCheckPointLength()
    {
        if (checkPoints.arraySize < 2)
        {
            while (checkPoints.arraySize < 2)
            {
                checkPoints.InsertArrayElementAtIndex(checkPoints.arraySize - 1);
            }
        }
    }

    void SetCheckPointNames()
    {
        for (int i = 0; i < checkPoints.arraySize; i++)
        {
            var prop = checkPoints.GetArrayElementAtIndex(i);
            var pointName = prop.FindPropertyRelative("zoneName");
            pointName.stringValue = "Checkpoint " + i;
        }
    }

    private void OnSceneGUI(SceneView sceneView)
    {
        DrawCheckPoints();
    }

    void DrawCheckPoints()
    {
        points = SerializedPropertyExtensions.GetValueFromScriptableObject<DetectZone[]>(checkPoints);
        for (int i = 0; i < points.Length; i++)
        {
            var pointName = points[i].zoneName;
            var zone = points[i];

            zone.DrawDetectZone(source, sourceRef);

            //drawlabel
            var style = new GUIStyle
            {
                fontStyle = FontStyle.Bold,
                alignment = TextAnchor.LowerCenter,
                normal = new GUIStyleState
                {
                    textColor = Color.black,
                }
            };
            Handles.Label(zone.worldPos + zone.offset, pointName, style);
        }
        
    }


}
