﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(UnitData))]
public class UnitDataEditor : Editor
{

    protected SerializedObject sourceRef;
    protected UnitData source;

    protected SerializedProperty spawnUI;
    protected SerializedProperty UIToSpawn;
    protected SerializedProperty skinPrefab;
    protected SerializedProperty avatarIcon;
    protected SerializedProperty skinSize;
    protected SerializedProperty weight;
    protected SerializedProperty skinRotation;
    protected SerializedProperty itemSpawnLoc;
    protected SerializedProperty itemSpawnInd;
    protected SerializedProperty speed;
    protected SerializedProperty jumpPower;
    protected SerializedProperty maxHP;
    protected SerializedProperty destroyOnDeath;
    protected SerializedProperty delay;
    protected SerializedProperty buffs;


    public virtual void OnEnable()
    {
        source = (UnitData)target;
        sourceRef = serializedObject;

        GetProperties();
    }

    public override void OnInspectorGUI()
    {
        SetProperties();
        sourceRef.ApplyModifiedProperties();
    }

    public virtual void GetProperties()
    {
        spawnUI = sourceRef.FindProperty("spawnUI");
        UIToSpawn = sourceRef.FindProperty("UIToSpawn");
        skinPrefab = sourceRef.FindProperty("skinPrefab");
        avatarIcon = sourceRef.FindProperty("avatarIcon");
        skinSize = sourceRef.FindProperty("skinSize");
        weight = sourceRef.FindProperty("weight");
        skinRotation = sourceRef.FindProperty("skinRotation");
        itemSpawnLoc = sourceRef.FindProperty("itemSpawnLoc");
        itemSpawnInd = sourceRef.FindProperty("itemSpawnInd");
        speed = sourceRef.FindProperty("speed");
        jumpPower = sourceRef.FindProperty("jumpPower");
        maxHP = sourceRef.FindProperty("maxHP");
        destroyOnDeath = sourceRef.FindProperty("destroyOnDeath");
        delay = sourceRef.FindProperty("delay");
        buffs = sourceRef.FindProperty("buffs");
    }

    public virtual void SetProperties()
    {

        EditorUtils.SpritePreviewField(avatarIcon, 80, 80, true);
        EditorGUILayout.PropertyField(spawnUI);
        if (spawnUI.boolValue)
            EditorUtils.PrefabFieldWithComponent(UIToSpawn, typeof(UIPlayer));
        EditorGUILayout.PropertyField(skinPrefab);
        EditorGUILayout.PropertyField(skinSize);
        EditorGUILayout.PropertyField(weight);
        EditorGUILayout.PropertyField(skinRotation);
        EditorUtils.DisplayAllChildrenPopup("Item Spawn Location", skinPrefab, itemSpawnInd, itemSpawnLoc);
        EditorGUILayout.PropertyField(maxHP);
        EditorGUILayout.PropertyField(speed);
        EditorGUILayout.PropertyField(jumpPower);   
        EditorGUILayout.PropertyField(destroyOnDeath);
        if (destroyOnDeath.boolValue)
            EditorGUILayout.PropertyField(delay);
        EditorGUILayout.PropertyField(buffs, true);
    }



}
