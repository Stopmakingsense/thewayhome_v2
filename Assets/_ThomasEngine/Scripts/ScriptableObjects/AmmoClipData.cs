﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "AmmoClip", menuName = "Data/Items/AmmoClip", order = 1)]
public class AmmoClipData : ScriptableObject
{
    public int clipSize;
}
