﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "UnitData", menuName = "Data/Units/UnitData", order = 1)]
public class UnitData : ScriptableObject
{
    public bool spawnUI;
    public UIUnit UIToSpawn;
    public GameObject skinPrefab;
    public Sprite avatarIcon;
    public float weight = 1;
    public Vector2 skinSize;
    public float skinRotation;
    public string itemSpawnLoc;
    public int itemSpawnInd;
    public float speed;
    public float jumpPower;
    public int maxHP;
    public bool destroyOnDeath;
    public float delay;
    public UnitBuff[] buffs;
}
