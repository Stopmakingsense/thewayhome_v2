﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.IO;

[CreateAssetMenu(fileName = "UserDataManager", menuName = "Data/Managers/UserDataManager", order = 1)]
public class UserDataManager : ScriptableObject
{
    [SerializeField] private int maxUsers = 4;
    public int MaxUsers { get { return maxUsers; } }
    public int curUserId;
    public List<User> users = new List<User>();
    private string userNameInput;
    private string jsonPath;

    #region DATA_WRITING

    public void InitializeData()
    {
        jsonPath = Application.streamingAssetsPath + "/UserManagerData.json";
        GetDataFromFile();
    }

    void GetDataFromFile()
    {
        if (!File.Exists(jsonPath))
            WriteDataToFile();
        else
            ReadDataFromFile();
    }

    void WriteDataToFile()
    {
        StreamWriter writer = new StreamWriter(jsonPath);
        string jsonData = StringSerializer.Serialize(typeof(UserDataManager), this);
        writer.Write(jsonData);
        writer.Close();
        Debug.Log("Saved Data To File");
    }

    void ReadDataFromFile()
    {
        //get data from json
        StreamReader reader = new StreamReader(jsonPath);
        string jsonData = reader.ReadToEnd();
        UserDataManager data = (UserDataManager)StringSerializer.Deserialize(typeof(UserDataManager), jsonData);
        //load data from file
        curUserId = data.curUserId;
        users.Clear();
        users.AddRange(data.users);
        //close reader
        reader.Close();
        Debug.Log("Reading data from file");
    }

    #endregion

    #region USER MANAGEMENT

    public void CreateUser(int _saveSlotInd)
    {
        if (NameExists(userNameInput))
            return;
        if (users.Count >= maxUsers)
        {
            Debug.Log("max capacity reached for users! Delete a user!");
            return;
        }


        User user = new User
        {
            userId = users.Count,
            playerName = userNameInput,
            saveSlotId = _saveSlotInd,
            levelUnlocked = 1
        };
        curUserId = user.userId;
        users.Add(user);

        WriteDataToFile();
    }

    public void RemoveUser(int _saveSlotInd)
    {
        User userToRemove = new User();
        foreach (var user in users)
        {
            if (user.saveSlotId == _saveSlotInd)
                userToRemove = user;
        }
        users.Remove(userToRemove);
        WriteDataToFile();
    }

    public void EraseAllUsers()
    {
        users.Clear();

        WriteDataToFile();
    }

    public void SetCurUser(int _id)
    {
        curUserId = _id;

        WriteDataToFile();
    }

    public User GetCurUser()
    {
        return users[curUserId];
    }

    #endregion

    #region USER NAME

    public string GetPlayerName()
    {
        return users[curUserId].playerName;
    }

    bool NameExists(string _name)
    {
        foreach (var user in users)
        {
            if (user.playerName == _name)
            {
                Debug.Log("Name Already Exists!");
                return true;
            }

        }
        return false;
    }

    public void SetUserName(string _name)
    {
        userNameInput = _name;
    }

    #endregion

    #region LEVELS/CHECKPOINTS

    public void SaveLevelName(string _levelName)
    {
        GetCurUser().levelName = _levelName;
        WriteDataToFile();
    }

    public string GetCurLevelName()
    {
        return GetCurUser().levelName;
    }

    public void SaveCheckPoint(int _checkPoint)
    {
        GetCurUser().curCheckPoint = _checkPoint;
        WriteDataToFile();
    }

    public int GetCurCheckPoint()
    {
        return GetCurUser().curCheckPoint;
    }

    public void UnlockLevel(int _level)
    {
        GetCurUser().levelUnlocked = _level;

        WriteDataToFile();
    }

    #endregion

    #region SKIN

    public void SetPlayerSkinData(int _skinInd)
    {
        users[curUserId].playerSkinInd = _skinInd;

        WriteDataToFile();
    }

    public PlayerSkinData GetSkinData()
    {
        PlayerSkinManager sm = GameManager.instance.GetSkinManager();
        return sm.playerSkins[users[curUserId].playerSkinInd];
    }

    #endregion

    #region COINS

    //public void Set

    #endregion

    #region INVENTORY

    public void SetInventoryItems(ItemData[] _invItems)
    {
        users[curUserId].inventoryItems = _invItems;
        Debug.Log("Saving quick menu items");

        WriteDataToFile();
    }

    #endregion

}
