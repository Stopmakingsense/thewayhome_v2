﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemUseableData : ItemData
{
    public enum AmmoType { UnlimitedClip, Reload, Recharge }
    public enum OnEmptyOption { Nothing, Destroy, Drop }

    public AmmoType ammoType;
    public OnEmptyOption onEmpty;
    public AmmoClipData ammoClip;
    public bool unlimitedTotalAmmo;
    public float reloadTime = 1;
    public float rechargeSpeed = 2;
    public bool overheat;
    public float overheatRechargeTime = 3;
}
