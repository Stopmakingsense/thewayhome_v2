﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "WeaponMelee", menuName = "Data/Items/Weapons/Melee", order = 1)]
public class ItemWeaponMeleeData : ItemData
{
    public enum DamageType { Box, Circle, DamageArea }
    public enum BounceType { ClosetPoints, Override, XOnly}
    public int damage = 1;
    public float damageDelay;
    public float activeTime = 1;
    public DamageType damageType;
    public string zone;
    public Vector2 offset;
    public Vector2 size;
    public float radius;
    public BounceType bounceType;
    public Vector2 direction;
    public float bounceForce = 1;
    public int unitAmount = 1;
    public LayerMask mask;
}
