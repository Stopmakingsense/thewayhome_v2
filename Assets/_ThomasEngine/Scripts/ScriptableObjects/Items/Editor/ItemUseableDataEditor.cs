﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Web;
using System;

[CustomEditor(typeof(ItemData))]
public class ItemUseableDataEditor : ItemDataEditor
{
    protected SerializedProperty ammoType;
    protected SerializedProperty onEmpty;
    protected SerializedProperty ammoClip;
    protected SerializedProperty unlimitedTotalAmmo;
    protected SerializedProperty reloadTime;
    protected SerializedProperty rechargeSpeed;
    protected SerializedProperty overheat;
    protected SerializedProperty overheatRechargeTime;

    public override void OnEnable()
    {
        base.OnEnable();
        source = (ItemUseableData)target;
        source.linkedType = typeof(ItemUseable);
    }

    public override void GetProperties()
    {
        base.GetProperties();
        ammoType = sourceRef.FindProperty("ammoType");
        onEmpty = sourceRef.FindProperty("onEmpty");
        ammoClip = sourceRef.FindProperty("ammoClip");
        unlimitedTotalAmmo = sourceRef.FindProperty("unlimitedTotalAmmo");
        reloadTime = sourceRef.FindProperty("reloadTime");
        rechargeSpeed = sourceRef.FindProperty("rechargeSpeed");
        overheat = sourceRef.FindProperty("overheat");
        overheatRechargeTime = sourceRef.FindProperty("overheatRechargeTime");
    }

    public override void SetProperties()
    {
        base.SetProperties();
        EditorGUILayout.LabelField("Usage Properties", boldStyle);
        EditorGUILayout.PropertyField(ammoType);
        if (ammoType.enumValueIndex == 0)
            return;

        EditorGUILayout.PropertyField(onEmpty);
        EditorGUILayout.PropertyField(ammoClip);
        //reload
        if (ammoType.enumValueIndex == 1)
        {
            EditorGUILayout.PropertyField(unlimitedTotalAmmo);
            EditorGUILayout.PropertyField(reloadTime);
        }
        //recharge
        else if (ammoType.enumValueIndex == 2)
        {
            EditorGUILayout.PropertyField(rechargeSpeed);
            EditorGUILayout.PropertyField(overheat);
            if (overheat.boolValue)
                EditorGUILayout.PropertyField(overheatRechargeTime);
        }
        //destroy or drop

    }
}
