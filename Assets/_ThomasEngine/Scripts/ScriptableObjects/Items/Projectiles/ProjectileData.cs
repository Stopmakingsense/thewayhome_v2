﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileData : LinkableData
{
    public enum DetectType { Circle, Box, LineCast }
    public DetectType detectType;
    public float lifeTime = 10;
    public Vector2 detectOffset = Vector2.zero;
    public float detectRadius = 0.25f;
    public Vector2 detectSize = Vector2.one;
    public int hitMaxAmount = 1;
    public InteractFX[] interactFX;
}
