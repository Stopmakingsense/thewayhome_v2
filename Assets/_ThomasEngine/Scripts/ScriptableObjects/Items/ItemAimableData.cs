﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemAimableData : ItemUseableData
{
    public float aimDistance = 10;
    public string muzzlePos;
    public int muzzlePosInd;
    public AimFX aimFX;
}
