﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Unit
{
    [SerializeField] private int maxLives = 3;

    private int curLives;
    public int CurLives { get { return curLives; } }
    [SerializeField] private bool ignorePhysicsOnHit = true;
    [SerializeField] private int ignoreLayerOne = 11;
    [SerializeField] private int ignoreLayerTwo = 17;
    [SerializeField] private float ignoreTime = 1;
    [SerializeField] private float stunTime = 1;
    private bool physicsIgnored;
    [SerializeField] private bool changeMeshMaterialOnHit;
    private Renderer[] meshesToChange;
    private Material[] startMeshesMaterials;
    [SerializeField] private Material materialToUse;
    private int curSkinInd;

    private int curPoints;
    public int CurPoints { get { return curPoints; } }

    public new UIPlayer UI { get { return (UIPlayer)ui;} }
    private UserDataManager dataManager;
    private PlayerSkinManager skinManager;
    private SpawnCheckPointManager sm;

    private PlayerSoundFX soundFX;
    private CapsuleCollider2D col;

    private GameObject curSkin;

    public override void Awake()
    {
        base.Awake();
        GetPlayerData();
    }

    public override void GetComponents()
    {
        base.GetComponents();
        //get components
        var gm = GameManager.instance;
        dataManager = gm.GetUserDataManager();
        skinManager = gm.GetSkinManager();
        sm = gm.GetSpawnManager();
        gm.SpawnedPlayer = this;
        col = GetComponent<CapsuleCollider2D>();
    }

    void GetPlayerData()
    {
        User user = dataManager.GetCurUser();
        curLives = user.lives;

        if (setData)
            curData = (PlayerSkinData)data;
        else
            curData = skinManager.playerSkins[user.playerSkinInd];

        SetPlayerSkin(curData);
        
        //get sound
        soundFX = curSkin.GetComponent<PlayerSoundFX>();
        if (anim)
            anim.Animator = (curSkin.GetComponent<Animator>());

        SetupPlayer();

    }

    public void IgnorePhysicsLayers()
    {
        StartCoroutine( StartIgnorePhysicsLayers());
    }

    IEnumerator StartIgnorePhysicsLayers()
    {
        physicsIgnored = true;
        SetHurtMeshMaterials(true);
        Physics2D.IgnoreLayerCollision(ignoreLayerOne,ignoreLayerTwo, true);
        float timer = 0;
        while (timer < ignoreTime)
        {
            timer += Time.deltaTime;
            if (timer > ignoreTime)
                timer = ignoreTime;

            yield return new WaitForEndOfFrame();
        }
        physicsIgnored = false;
        SetHurtMeshMaterials(false);
        Physics2D.IgnoreLayerCollision(ignoreLayerOne, ignoreLayerTwo, false);

    }

    void GetMaterials()
    {
        if (!changeMeshMaterialOnHit)
            return;

        meshesToChange = curSkin.GetComponentsInChildren<Renderer>();
        if (meshesToChange.Length > 0)
        {
            startMeshesMaterials = new Material[meshesToChange.Length];
            for (int i = 0; i < meshesToChange.Length; i++)
            {
                startMeshesMaterials[i] = meshesToChange[i].material;
            }
        }
        else
            Debug.LogError("No Meshrenderers found on " + curSkin);

    }

    void SetHurtMeshMaterials(bool _hurt)
    {
        if (!changeMeshMaterialOnHit)
            return;

        if (_hurt)
        {
            for (int i = 0; i < meshesToChange.Length; i++)
            {
                if (meshesToChange[i])
                    meshesToChange[i].material = materialToUse;
            }
        }
        else
        {
            for (int i = 0; i < meshesToChange.Length; i++)
            {
                if (meshesToChange[i])
                    meshesToChange[i].material = startMeshesMaterials[i];
            }
        }

    }

    public void SetPlayerSkin(UnitData _skinData)
    {
        base.SetData(_skinData);

        //destroy current skin
        if (curSkin)
            Destroy(curSkin);

        //spawn new skin
        curSkin = Instantiate(curData.skinPrefab, transform.position, transform.rotation);
        curSkin.transform.SetParent(transform);

        //set Rotation
        curSkin.transform.localEulerAngles = new Vector2(transform.rotation.x, transform.rotation.y + curData.skinRotation);

        //set collider size
        col.size = curData.skinSize;
        col.offset = new Vector2(0, col.size.y / 2);

        //update data
        if (skinManager.playerSkins.IndexOf((PlayerSkinData)curData) != -1)
            dataManager.SetPlayerSkinData(skinManager.playerSkins.IndexOf((PlayerSkinData)curData));
        else
            Debug.Log("Make sure you add " + _skinData + " to the skin manager: " + skinManager + "!");

    }

    public void SetupPlayer()
    {
        curHp.Value = maxHp;

        //set UI stats
        if (ui)
        {
            ui.SetHPValue(curHp.Value);
            UI.SetLivesValue(curLives);
        }

        dead = false;

        //update anim
        if (anim)
            anim.PlayIdle();

        //for ignore physics
        GetMaterials();

    }

    public void AddPoints(int _amount)
    {
        curPoints += _amount;
        if (UI)
            UI.SetPointsValue(curPoints);
    }

    public override void DamageHp(float _damage)
    {
        base.DamageHp(_damage);
        ApplyDamage(_damage, 0, Vector2.zero, false);
    }

    public void DamageHp(float _damage, float _bounceForce, Vector2 _direction, bool _stunMovement)
    {
        base.DamageHp(_damage, _bounceForce, _direction);
        ApplyDamage(_damage, _bounceForce, _direction, _stunMovement);
    }

    void ApplyDamage(float _damage, float _bounceForce, Vector2 _direction, bool _stunMovement)
    {
        if (dead) //dont apply damage if fade is in effect or dead
            return;

        if (physicsIgnored)
            return;

        //stun player movement if grounded
        if (controller.IsGrounded && _stunMovement)
            controller.DisableMovement(stunTime);

        //play hurt sound
        if (soundFX)
            soundFX.PlayHurtSound();

        //sync anim for one frame
        if (anim)
            anim.PlayHurt();

        if (ignorePhysicsOnHit)
            //ignore physics
            IgnorePhysicsLayers();

        Debug.Log("damaging player by " + _damage + " at " + _direction + " direction.");
        
    }

    public override void Die(string _reason)
    {
        base.Die();

        //update anim
        if (anim)
            anim.PlayDead();

        curHp.Value = 0;
        curLives--;
        if (curLives > 0)
            Respawn();
        else
        {
                UI.SetLivesValue(curLives);
                GameManager.instance.GameOverLose();
        }

        Debug.Log(gameObject + " has died from " + _reason + "!");


        //play death sound
        if (soundFX)
            soundFX.PlayDeathSound();
    }

    public void Respawn()
    {
        sm.RespawnPlayer();
    }

    public PlayerSoundFX GetSoundFX()
    {
        return soundFX;
    }

    public int GetCurSkinInd()
    {
        return curSkinInd;
    }

    public int GetCurLives()
    {
        return curLives;
    }

    public bool IsPhysicsIgnored()
    {
        return physicsIgnored;
    }
}
