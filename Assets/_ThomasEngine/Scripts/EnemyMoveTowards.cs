﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Enemy))]
[RequireComponent(typeof(EnemyController))]
public class EnemyMoveTowards : UnitMoveTowards
{
    //arrival options
    [SerializeField] private bool useItem;

    private EnemyController enc;
    private UnitEquip equip;

    public override void Start()
    {
        base.Start();
        
    }

    public override void GetComponents()
    {
        base.GetComponents();
        enc = (EnemyController)controller;
        equip = GetComponent<UnitEquip>();
    }

    public override void MoveTowardsTarget (GameObject _target)
    {
        enc.SetAimTarget(_target.GetComponent<Unit>().AttackTarget);
        enc.PausePatrol(true);
        base.MoveTowardsTarget(_target); 
    }

    public override void DoArrivalEvents()
    {
        base.DoArrivalEvents();
        if (useItem)
        {
            equip.UseEquippedItem();
        }  
        StartCoroutine(StartCheckDistance());

    }

    public override void StopChasing()
    {
        base.StopChasing();
        enc.PausePatrol(false);
        enc.SetAimTarget(null);
    }

    void StopAttacking()
    {
        if (useItem)
        {
            equip.StopUseEquippedItem();
        }
    }

    IEnumerator StartCheckDistance()
    {
        while (arrived)
        {
            CheckTargetDistance();
            yield return new WaitForEndOfFrame();
        }
        StopAttacking();
        StopChasing();
        MoveTowardsTarget(target.gameObject);
    }
}