﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ItemUseable : Item
{
    public new ItemUseableData Data { get { return (ItemUseableData)data; } }
    protected float curClipSize;
    public float ClipSize { get { return curClipSize; } }
    protected FloatWrapper curClipAmount = new FloatWrapper(0);
    public float ClipAmount { get { return curClipAmount.Value; } }
    protected int curClipInd;
    public int ClipInd { get { return curClipInd; } }
    protected float totalAmount;
    public float TotalAmount { get { return totalAmount; } }

    protected bool reloading;
    public bool IsReloading { get { return reloading; } }
    protected bool overheated;
    public bool IsOverheated { get { return overheated; } }
    protected bool recharging;
    public bool IsRecharging { get { return recharging; } }
    protected bool empty;
    public bool IsEmpty { get { return empty; } }

    public override void Awake()
    {
        base.Awake();
    }

    public override void Start()
    {
        base.Start();
    }

    public override void OnEnable()
    {
        base.OnEnable();
    }

    public override void OnDisable()
    {
        base.OnDisable();
        if (curUI)
            Destroy(curUI.gameObject);
    }

    public virtual void UseItem()
    {

    }

    public virtual void StopUseItem()
    {

    }

    public override void InitializeItem()
    {
        base.InitializeItem();
        if (!dropped || Data.keepUIActiveIfDropped)
            SetupUI();
    }

    public override void LoadDefaultData()
    {
        base.LoadDefaultData();
        if (dropped)
            return;
        curClipAmount.Value = 0;
        curClipInd = 0;
        totalAmount = 0;
        if (Data.ammoClip)
            curClipSize = Data.ammoClip.clipSize;
        AddAmount(curClipSize);
    }

    public virtual void AddToClipSize(float _amount)
    {
        curClipSize += _amount;

        if (curUI)
            curUI.SetItemAmountMinMaxValue(0, curClipSize);
    }

    public virtual void RemoveMaxValue(float _amount)
    {
        curClipSize -= _amount;

        if (curUI)
            curUI.SetItemAmountMinMaxValue(0, curClipSize);
    }

    public virtual void AddAmount(float _amount)
    {
        totalAmount += _amount;
        RefreshClipAmount();
        if (curClipAmount.Value > curClipSize)
        {
            if (Data.ammoType == ItemUseableData.AmmoType.Reload)
            {
                curClipInd++;
            }
            else
                totalAmount = curClipSize;
        }

        RefreshClipAmount();

        if (curUI)
            curUI.SetItemAmountValue(curClipAmount.Value);
    }

    public virtual void RemoveAmount(float _amount)
    {
        if (Data.ammoType == ItemUseableData.AmmoType.UnlimitedClip)
            return;

        totalAmount -= _amount;
        RefreshClipAmount();
        if (curClipAmount.Value <= 0)
        {
            if (Data.ammoType == ItemUseableData.AmmoType.Reload)
            {
                Reload();
            }
            else if (Data.ammoType == ItemUseableData.AmmoType.Recharge)
            {
                OverHeat();
            }
            else
            {
                OnEmpty();
            }
                
        }
        else if (Data.ammoType == ItemUseableData.AmmoType.Recharge)
        {
            Recharge();
        }

        if (curUI)
            curUI.SetItemAmountValue(curClipAmount.Value);
    }

    void Reload()
    {
        if (Data.unlimitedTotalAmmo)
        {
            totalAmount = curClipSize;
            if (!reloading)
                StartCoroutine(StartReload());
        }
        else if (curClipInd > 0)
        {
            curClipInd--;
            if (!reloading)
                StartCoroutine(StartReload());
        }
    }

    void Recharge()
    {
        if (!recharging)
            StartCoroutine(StartRecharge());
    }

    void OverHeat()
    {
        StartCoroutine(StartOverheat());
    }

    void RefreshClipAmount()
    {
        curClipAmount.Value = totalAmount - (curClipSize * curClipInd);
        empty = curClipAmount.Value <= 0;
    }

    IEnumerator StartRecharge()
    {
        recharging = true;
        while (curClipAmount.Value < curClipSize)
        {
            AddAmount(Data.rechargeSpeed * Time.deltaTime);
            yield return new WaitForEndOfFrame();
        }
        recharging = false;
    }

    IEnumerator StartOverheat()
    {
        overheated = true;
        float timer = 0;
        float perc = 0;
        //do UI stuff
        if (curUI)
            curUI.ActivateReloadAmount(true);
        while (perc < 1)
        {
            timer += Time.deltaTime;
            if (timer > Data.reloadTime)
                timer = Data.reloadTime;
            perc = timer / Data.reloadTime;

            //do UI stuff
            if (curUI)
                curUI.SetReloadAmountValue(perc);
            yield return new WaitForEndOfFrame();
        }
        overheated = false;
        //do UI stuff
        if (curUI)
        {
            curUI.SetItemAmountValue(curClipAmount.Value);
            curUI.ActivateReloadAmount(false);
        }
    }

    IEnumerator StartReload()
    {
        reloading = true;
        float timer = 0;
        float perc = 0;
        //do UI stuff
        if (curUI)
            curUI.ActivateReloadAmount(true);
        while (perc < 1)
        {
            timer += Time.deltaTime;
            if (timer > Data.reloadTime)
                timer = Data.reloadTime;
            perc = timer / Data.reloadTime;

            //do UI stuff
            if (curUI)
                curUI.SetReloadAmountValue(perc);

            yield return new WaitForEndOfFrame();
        }
        RefreshClipAmount();
        reloading = false;
        //do UI stuff
        if (curUI)
        {
            curUI.SetItemAmountValue(curClipAmount.Value);
            curUI.ActivateReloadAmount(false);
        }
            
    }

    public virtual void OnEmpty()
    {
        empty = true;
        totalAmount = 0;
        RefreshClipAmount();

        if (Data.onEmpty == ItemUseableData.OnEmptyOption.Destroy)
        {
            KillItem();
        }
        else if (Data.onEmpty == ItemUseableData.OnEmptyOption.Drop)
        {
            var equip = curUnitOwner.GetComponent<UnitEquip>();
            equip.DropCurrentItem();
        }
    }

    public virtual void PickUpAmmo(AmmoClipData _ammoData)
    {
        if (_ammoData != Data.ammoClip)
            return;
        AddAmount(_ammoData.clipSize);

    }

    public override void CopyValues(Item _item)
    {
        base.CopyValues(_item);
        var useable = (ItemUseable)_item;
        if (useable)
        {
            curClipInd = useable.ClipInd;
            curClipSize = useable.ClipSize;
            curClipAmount.Value = useable.ClipAmount;
            totalAmount = useable.TotalAmount;
        }

    }

    public virtual void SetupUI()
    {
        if (!curUI && Data.spawnUI)
        {
            curUI = Instantiate(Data.itemUI).GetComponent<UIItem>();
            if (Data.setOwnerAsParent)
            {
                var trans = transform;
                if (curUnitOwner)
                    trans = curUnitOwner.transform;
                curUI.transform.position = trans.position;
                curUI.transform.rotation = Quaternion.identity;
                curUI.transform.SetParent(trans);
            }
            curUI.SetItemAmountMinMaxValue(0, curClipSize);
            curUI.SetItemAmountValue(curClipAmount.Value);
        }
    }

}
