﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemDrawable : ItemAimable
{
    public new ItemDrawableData Data { get { return (ItemDrawableData)data; } }
    private Coroutine drawRoutine;

    private List<Vector2> drawPoints = new List<Vector2>();

    private TrailRenderer trail;

    private bool drawing;

    public override void OnEnable()
    {
        base.OnEnable();
    }

    public override void OnDisable()
    {
        base.OnDisable();
        drawPoints.Clear();
        if (trail)
            Destroy(trail.gameObject);
    }

    public override void UseItem()
    {
        if (curClipAmount.Value <= 0)
            return;

        if (Data.useMask)
        {
            if (!Physics2D.OverlapPoint(controller.AimPos, Data.mask))
            {
                Debug.Log("Not a drawable area!");
                return;
            }
        }

        trail = Instantiate(Data.trailRenderer, controller.AimPos, Quaternion.identity).GetComponent<TrailRenderer>();
        drawRoutine = StartCoroutine(StartDrawing());
    }

    public override void StopUseItem()
    {
        drawing = false;
    }

    void GrowLine()
    {
        if (drawPoints.Count < 1)
            return;

        StartCoroutine(StartGrowLine());
    }

    IEnumerator StartDrawing()
    {
        drawing = true;
        float addedDistance = 0;
        Vector2 lastPos = controller.AimPos;
        float aimdist = 0;
        while (curClipAmount.Value > 0 && drawing && aimdist < Data.aimDistance)
        {
            aimdist = Vector2.Distance(muzzle.position, controller.AimPos);
            trail.transform.position = controller.AimPos;
            addedDistance = Vector2.Distance(lastPos, controller.AimPos);
            RemoveAmount(addedDistance);
            if (lastPos != controller.AimPos)
                AddDrawPoint(controller.AimPos); 
            lastPos = controller.AimPos;
            yield return new WaitForFixedUpdate();
        }
        GrowLine();  
    }

    IEnumerator StartGrowLine()
    {
        int i = 0;
        var seed = Instantiate(Data.topPrefab, drawPoints[i], Quaternion.identity);
        float timer = 0;
        float distanceTravelled = 0;
        var lastPos = drawPoints[i];
        while (i < drawPoints.Count - 1)
        {
            //get drawpoints data
            var pos = drawPoints[i];
            var dest = drawPoints[i + 1];
            var dir = (drawPoints[i + 1] - drawPoints[i]).normalized;
            var distance = Vector2.Distance(pos, dest);

            //timer data
            var time = distance / Data.growSpeed;
            timer += Time.deltaTime;
            float perc = timer / time;

            //move the seed
            seed.transform.up = dir;
            seed.transform.position = Vector2.Lerp(pos, dest, perc);

            //how far has the seed moved?
            distanceTravelled += Vector2.Distance(lastPos, seed.transform.position);

            //spawn a vine after amount travelled
            if (distanceTravelled > Data.sensitivityDistance || i == 0)
            {
                SpawnLinePiece(seed.transform.position, dir);
                distanceTravelled = 0;
            }

            //go through all positions
            if (perc >= 1)
            {
                perc = 0;
                timer = 0;
                i++;
            }
                   
            //store last seed position for next loop
            lastPos = seed.transform.position;
            yield return new WaitForEndOfFrame();
        }
        drawPoints.Clear();
        if (trail)
            Destroy(trail.gameObject);
    }

    void AddDrawPoint(Vector2 _pos)
    {
        if (drawPoints.Count > 0)
        {
            var dist = Vector2.Distance(_pos, drawPoints[drawPoints.Count - 1]);
            if (dist > Data.sensitivityDistance)
                drawPoints.Add(_pos);
        }
        else
            drawPoints.Add(_pos);
    }

    void SpawnLinePiece(Vector2 _pos, Vector2 _dir)
    {
        var spawn = Instantiate(Data.growLinePrefab, _pos, Quaternion.identity);
        spawn.transform.up = _dir;
    }
}
