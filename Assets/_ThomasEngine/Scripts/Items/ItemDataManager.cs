﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "InventoryManager", menuName = "Managers/InventoryManager", order = 1)]
public class ItemDataManager : ScriptableObject
{
    public List<ItemData> items = new List<ItemData>();
}
