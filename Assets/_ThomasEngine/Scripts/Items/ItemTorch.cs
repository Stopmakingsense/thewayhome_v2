﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Light))]
public class ItemTorch : ItemAimable
{
    public new ItemTorchData Data { get { return (ItemTorchData)data; } }
    private Light lightSource;
    public float CurLightRange { get { return lightSource.range; } }
    public float CurLightIntensity { get { return lightSource.intensity; } }

    private Coroutine lightDrainRoutine;
    private Coroutine hpDrainRoutine;

    public override void Start()
    {
        base.Start();
        SetLightProperties();
    }

    public override void OnEnable()
    {
        base.OnEnable();
        if (!lightSource)
            lightSource = GetComponent<Light>();
        SetLightProperties();
    }

    public override void UseItem()
    {
    }

    public override void StopUseItem()
    {
    }

    public override void RemoveAmount(float _amount)
    {
        base.RemoveAmount(_amount);
        SetLightProperties();
    }

    public override void AddAmount(float _amount)
    {
        base.AddAmount(_amount);
        SetLightProperties();
    }

    void SetLightProperties()
    {
        if (!lightSource)
            return;
        var perc = curClipAmount.Value / curClipSize;
        lightSource.range = Mathf.Lerp(0, Data.lightMaxRange, perc);
        lightSource.intensity = Mathf.Lerp(0, curClipSize, perc);
    }

    public void EnableLightDrain(bool _enable)
    {
        if (_enable)
            lightDrainRoutine = StartCoroutine(StartDrain(curClipAmount, 0, Data.lightDrainSpeed));
        else if (lightDrainRoutine != null)
            StopCoroutine(lightDrainRoutine);
    }

    public void EnableHealthDrain(bool _enable)
    {
        if (_enable)
            hpDrainRoutine = StartCoroutine(StartHpDrain(lastUnitOwner.CurHP, 0, Data.hpDrainSpeed));
        else if (hpDrainRoutine != null)
            StopCoroutine(hpDrainRoutine);
    }

    IEnumerator StartDrain(FloatWrapper _curValue, float _targetValue, float _speed)
    {
        while (_curValue.Value > _targetValue)
        {
            RemoveAmount(_speed * Time.deltaTime);
            if (_curValue.Value < _targetValue)
                _curValue.Value = _targetValue;
            yield return new WaitForEndOfFrame();
        }
    }

    IEnumerator StartHpDrain(FloatWrapper _curValue, float _targetValue, float _speed)
    {
        while (_curValue.Value > _targetValue)
        {
            lastUnitOwner.DamageHp(_speed * Time.deltaTime);
            if (_curValue.Value < _targetValue)
                _curValue.Value = _targetValue;
            yield return new WaitForEndOfFrame();
        }
    }

    public override void AddBuff(ItemBuff _buffToAdd)
    {
        base.AddBuff(_buffToAdd);
        SetLightProperties();
    }

    public override void RemoveBuff(ItemBuff _buffToRemove)
    {
        base.RemoveBuff(_buffToRemove);
        SetLightProperties();
    }

}
