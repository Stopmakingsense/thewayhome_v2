﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ItemAimable : ItemUseable
{
    public new ItemAimableData Data { get { return (ItemAimableData)data; } }
    protected Transform muzzle;
    public Transform Muzzle { get { return muzzle; } }
    protected LineRenderer line;
    protected Color startColor;
    protected Color endColor;
    protected Transform aimRef;
    protected UnitController controller;


    public override void Start()
    {
        base.Start();     
    }

    public override void OnDisable()
    {
        base.OnDisable();
        if (line)
            Destroy(line.gameObject);
        if (aimRef)
            Destroy(aimRef.gameObject);
    }

    public virtual void FixedUpdate()
    {
        if (!controller)
            return;

        DrawAimerReference();
    }

    public override void InitializeItem()
    {
        base.InitializeItem();
        if (dropped)
            return;
        controller = curUnitOwner.GetComponent<UnitController>();
        muzzle = transform.Find(Data.muzzlePos);
        SpawnAimerReference();
    }

    void SpawnAimerReference()
    {
        if (!Data.aimFX)
            return;

        if (Data.aimFX.lineRenderer)
        {
            line = Instantiate(Data.aimFX.lineRenderer, muzzle.position, Quaternion.identity).GetComponent<LineRenderer>();
            startColor = line.startColor;
            endColor = line.endColor;
            if (Data.aimFX.lineType == AimFX.LineType.Firefade)
                SetLineAlpha(0);
        }

        if (Data.aimFX.aimRetical && controller)
            aimRef = Instantiate(Data.aimFX.aimRetical, controller.AimPos, Quaternion.identity).transform;

    }

    void DrawAimerReference()
    {
        if (!Data.aimFX)
            return;

        PositionRetical();
        if (Data.aimFX.lineType == AimFX.LineType.Constant)
            DrawLine(controller.AimPos);
    }

    void PositionRetical()
    {
        if (!aimRef)
            return;

        aimRef.position = controller.AimPos;
    }

    void DrawLine(Vector2 _endPos)
    {
        if (!line)
            return;

        line.SetPosition(0, muzzle.position);
        line.SetPosition(1, _endPos);
    }

    public virtual void DoLineFX(Vector2 _endPos)
    {
        if (line)
        {
            if (Data.aimFX.lineType == AimFX.LineType.Firefade)
                StartCoroutine(StartLineFade(_endPos));
        }
    }

    IEnumerator StartLineFade(Vector2 _endPos)
    {
        DrawLine(_endPos);
        float timer = 0;
        while (timer < Data.aimFX.fadeTime)
        {
            timer += Time.deltaTime;
            var perc = timer / Data.aimFX.fadeTime;
            SetLineAlpha(Mathf.Lerp(startColor.a, 0, perc));
            yield return new WaitForEndOfFrame();
        }
    }

    void SetLineAlpha(float _value)
    {
        var start = new Color(startColor.r, startColor.g, startColor.b, _value);
        var end = new Color(endColor.r, endColor.g, endColor.b, _value);
        line.startColor = start;
        line.endColor = end;
    }

}
