﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemWeaponRanged : ItemAimable
{
    public new ItemWeaponRangedData Data { get { return (ItemWeaponRangedData)data; } }

    private Coroutine fireCoroutine;
    private bool recoiling;

    public override void Start()
    {
        base.Start();     
    }

    public override void OnDisable()
    {
        if (line)
            Destroy(line.gameObject);
        if (aimRef)
            Destroy(aimRef.gameObject);
    }

    public override void UseItem()
    {
        if (!recoiling)
           fireCoroutine = StartCoroutine(StartFireWeapon());
    }

    public override void StopUseItem()
    {
        if (fireCoroutine != null)
            StopCoroutine(fireCoroutine);
    }

    IEnumerator StartFireWeapon()
    {
        FireWeapon();
        while (true)
        {
            if (!recoiling && !reloading && !empty && !overheated)
                FireWeapon();
            yield return new WaitForEndOfFrame();
        }
    }

    IEnumerator StartRecoiling()
    {
        recoiling = true;
        yield return new WaitForSeconds(Data.fireDelay);
        recoiling = false;
    }

    void FireWeapon()
    {
        Utils.LookAt2D(muzzle, controller.AimPos, true);

        //fx
        if (Data.particleFX)
            Instantiate(Data.particleFX, muzzle.position, muzzle.rotation);

        if (Data.spreadType != ItemWeaponRangedData.SpreadType.Straight)
        {
            float ang = Data.angle / (Data.fireAmount - 1);
            float curRot = Data.angle / 2;
            if (Data.fireAmount > 1)
            {
                for (int i = 0; i < Data.fireAmount; i++)
                {
                    if (Data.spreadType == ItemWeaponRangedData.SpreadType.Random)
                    {
                        ang = Random.Range(ang - Data.randomAmount, ang + Data.randomAmount);
                        curRot = Random.Range(curRot - Data.randomAmount, curRot + Data.randomAmount);
                    }
                    if (i == 0)
                        muzzle.Rotate(0, 0, -curRot);
                    else
                        muzzle.Rotate(0, 0, ang);

                    Shoot();
                }
            }
        }
        else
        {
            Shoot();
        }
        DoLineFX(controller.AimPos);
        //remove ammo amount
        RemoveAmount(1);

        //do recoil
        StartCoroutine(StartRecoiling());

    }

    void Shoot()
    {
        if (Data.fireType == ItemWeaponRangedData.FireType.Instant)
            ShootInstant();
        else
            ShootProjectile();
    }

    void ShootProjectile()
    {
        var proj = Instantiate(Data.projectile, muzzle.position, muzzle.rotation).GetComponent<Projectile>();
        var col = Physics2D.OverlapPoint(controller.AimPos, Data.mask);
        Transform tar = null;
        if (col)
            tar = col.transform;
        proj.ShootProjectile(Data.projectileSpeed, Data.damage,controller.AimDirection, Data.mask, transform, tar, controller.AimPos);
    }

    void ShootInstant()
    {
        RaycastHit2D hit = Physics2D.Raycast(muzzle.position, muzzle.forward, Data.fireDistance, Data.mask);
        if (hit)
        {
            var unit = hit.collider.GetComponent<Unit>();
            if (unit)
                unit.DamageHp(Data.damage);
        }
    }

}
