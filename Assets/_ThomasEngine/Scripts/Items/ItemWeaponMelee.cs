﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemWeaponMelee : ItemUseable
{
    public new ItemWeaponMeleeData Data { get { return (ItemWeaponMeleeData)data; } }
    private DamageArea da;
    private Transform rootPos;
    private UnitController rootCont;
    private bool damageActive;
    private Collider2D[] cols;
    private List<Collider2D> damagedCols = new List<Collider2D>();


    public override void Start()
    {
        base.Start();
        if (Data.damageType == ItemWeaponMeleeData.DamageType.DamageArea)
            da = GetComponent<DamageArea>();

        rootPos = transform.root;
        rootCont = rootPos.GetComponent<UnitController>();
        var unit = rootPos.GetComponentInChildren<Unit>();
        if (unit)
            rootPos = unit.transform;
    }

    public virtual void Update()
    {
        if (Input.GetButtonDown("Fire1"))
            UseItem();
    }

    public override void UseItem()
    {
        if (da)
            da.SetZoneActive(Data.zone, true, Data.activeTime, Data.damageDelay);
        else if (!damageActive)
        {
            StopAllCoroutines();
            StartCoroutine(StartAttack());
        }

    }

    public override void StopUseItem()
    {
    }

    IEnumerator StartAttack()
    {
        damageActive = true;
        yield return new WaitForSeconds(Data.damageDelay);
        float timer = 0;
        while (timer < Data.activeTime && cols.Length < Data.unitAmount)
        {
            Attack();
            yield return new WaitForEndOfFrame();
        }
        damagedCols.Clear();
        damageActive = false;
    }

    void Attack()
    {
        Vector2 offset = (Vector2)rootPos.transform.position + Data.offset;
        if (!rootCont.IsFacingRight)
            offset = new Vector2(-offset.x, offset.y);
        if (Data.damageType == ItemWeaponMeleeData.DamageType.Box)
            cols = Physics2D.OverlapBoxAll(offset, Data.size, 0, Data.mask);
        else
            cols = Physics2D.OverlapCircleAll(offset, Data.radius, Data.mask);

        if (cols.Length > 0)
        {
            for (int i = 0; i < Data.unitAmount; i++)
            {
                if (!damagedCols.Contains(cols[i]))
                {
                    Vector2 dir = Data.direction;
                    if (Data.bounceType == ItemWeaponMeleeData.BounceType.ClosetPoints)
                        dir = ((Vector2)cols[i].bounds.center - offset).normalized;
                    else if (Data.bounceType == ItemWeaponMeleeData.BounceType.XOnly)
                    {
                        if (rootPos.transform.position.x < cols[i].transform.position.x)
                            dir = Vector2.right;
                        else
                            dir = Vector2.left;
                    }

                    var u = cols[i].GetComponent<Unit>();
                    if (u)
                    {
                        DamageUnit(u, dir);
                        damagedCols.Add(cols[i]);
                    }
                }      
            }
        }
        
    }

    void DamageUnit(Unit _unit, Vector2 _dir)
    {
        _unit.DamageHp(Data.damage);
        var cont = _unit.GetComponent<UnitController>();
        if (cont)
        {
            cont.Bounce(_dir, Data.bounceForce);
        }
            
    }

}
