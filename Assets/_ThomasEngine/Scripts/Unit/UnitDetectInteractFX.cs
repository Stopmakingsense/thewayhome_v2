﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitDetectInteractFX : UnitDetect
{
    [SerializeField] private InteractFX[] interacts;

    public override void OnEnter()
    {
        base.OnEnter();
        foreach (var fx in interacts)
        {
            fx.DoFX(gameObject, col.gameObject);
        }
    }

}
