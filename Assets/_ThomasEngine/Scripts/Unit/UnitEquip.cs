﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitEquip : MonoBehaviour
{
    [SerializeField] protected bool autoEquipItems;
    [SerializeField] protected bool setAllItemsActive;
    [SerializeField] protected int maxItems = 4;
    [SerializeField] protected bool setInventoryItems;
    [SerializeField] protected ItemData[] itemDatas;
    [SerializeField] protected float dropThrowPower = 5;


    protected Unit unit;
    protected UnitController controller;
    protected Transform spawnLoc;

    protected ItemUseable curItem;
    public ItemUseable CurItem { get { return curItem; } }

    protected GameObject[] curItems;

    protected int curInd;
    protected bool equipped;

    public virtual void Start()
    {
        GetComponents();
        GetSpawnLocation();
        SpawnItems();
    }

    public virtual void GetComponents()
    {
        unit = GetComponent<Unit>();
        controller = GetComponent<UnitController>();
    }

    public virtual void GetSpawnLocation()
    {
        spawnLoc = Utils.FindDeepChild(transform, unit.curUnitData.itemSpawnLoc);
    }

    public virtual void SpawnItems()
    {
        if (setInventoryItems && itemDatas.Length > 0)
        {
            curItems = new GameObject[itemDatas.Length];
            for (int i = 0; i < itemDatas.Length; i++)
            {
                AddItem(itemDatas[i]);
            }
        }
        else
        {
            curItems = new GameObject[maxItems];
        }
    }

    public virtual void UseEquippedItem()
    {
        if (!curItem)
            return;

        curItem.UseItem();
    }

    public virtual void StopUseEquippedItem()
    {
        if (!curItem)
            return;

        curItem.StopUseItem();
    }

    public virtual void AddItem(Item _item)
    {
        for (int i = 0; i < curItems.Length; i++)
        {
            if (curItems[i] == null)
            {
                var data = _item.Data;
                //add to data list
                itemDatas[i] = data;
                //spawn item into the spawn location
                var item = Instantiate(data.connectedPrefab, spawnLoc.position, spawnLoc.rotation).GetComponent<Item>();
                //set item owner
                item.SetOwner(unit);
                //set item amount if item is picked up
                item.CopyValues(_item);
                //spawn item in proper spot
                item.transform.SetParent(spawnLoc);
                //"pick up" item
                item.PickUp();
                //add to items prefab list
                curItems[i] = item.gameObject;

                //set object invisiable
                if (!setAllItemsActive)
                    item.gameObject.SetActive(false);
                else
                    SetAllItemsActive();

                if (!curItem)
                    SetCurItem(0);

                return;
            }
        }
    }

    //adding default item to list
    public virtual void AddItem(ItemData _itemData)
    {
        if (!_itemData)
            return;
        //get item
        var item = _itemData.connectedPrefab.GetComponent<Item>();
        //load default data before spawn
        item.LoadDefaultData();
        AddItem(item);
    }

    public virtual void RemoveCurrentItem()
    {
        itemDatas[curInd] = null;
        equipped = false;
        if (curItem)
        {
            Destroy(curItem.gameObject);
            curItems[curInd] = null;
            curItem = null;
        }
        if (autoEquipItems)
        {
            curItem = FindItemToEquip();
            EquipCurItem(true);
        }

    }

    public virtual void RemoveItem(ItemData _itemToRemove)
    {
        for (int i = 0; i < itemDatas.Length; i++)
        {
            if (itemDatas[i] == _itemToRemove)
            {
                RemoveItem(i);
                return;
            }

        }
    }

    public virtual void RemoveItem(int _ind)
    {
        itemDatas[_ind] = null;
        Destroy(curItems[_ind]);
    }

    public virtual void DropCurrentItem()
    {
        if (!curItem)
            return;
        if (!curItem.Data.droppable)
            return;
        //instantiate cloned item and "drop" it
        var item = Instantiate(curItem.Data.droppedPrefab, curItem.transform.position, curItem.transform.rotation)
            .GetComponent<ItemUseable>();
        item.CopyValues(curItem);
        item.SetOwner(null);
        item.Drop();
        //push object away if it has rigidbody
        var irb = item.GetComponent<Rigidbody2D>();
        if (irb)
            irb.AddForce(controller.AimDirection * dropThrowPower, ForceMode2D.Impulse);
        //remove current item
        RemoveCurrentItem(); 
    }

    ItemUseable FindItemToEquip()
    {
        for (int i = 0; i < curItems.Length; i++)
        {
            if (curItems[i] != null)
            {
                curInd = i;
                return curItems[curInd].GetComponent<ItemUseable>();
            }
                
        }
        return null;
    }

    public virtual void EquipCurItem(bool _equip)
    {
        if (!curItem)
            return;
        equipped = _equip;
        curItem.gameObject.SetActive(equipped);
    }

    public virtual void SetAllItemsActive()
    {
        foreach (var item in curItems)
        {
            if (item != null)
                item.SetActive(true);
        }
    }

    public virtual void SwitchActiveItem(int _itemInd)
    {
        for (int i = 0; i < curItems.Length; i++)
        {
            if (curItems[i] != null)
            {
                curItems[i].SetActive(i == _itemInd);
            }
            
        }
    }

    public virtual void SetCurItem(int _itemInd)
    {
        for (int i = 0; i < curItems.Length; i++)
        {
            if (i == _itemInd)
            {
                curInd = i;
                curItem = curItems[i].GetComponent<ItemUseable>();
                if (autoEquipItems || equipped )
                {
                    if (!setAllItemsActive)
                        SwitchActiveItem(i);
                    EquipCurItem(true);
                }
            }     

        }     
    }

    
}
