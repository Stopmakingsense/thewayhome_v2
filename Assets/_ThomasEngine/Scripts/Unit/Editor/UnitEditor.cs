﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Unit))]
public class UnitEditor : Editor
{
    protected SerializedObject sourceRef;
    //skins
    protected SerializedProperty setData;
    protected SerializedProperty data;
    //enemy configs
    protected SerializedProperty attackTarget;


    public virtual void OnEnable()
    {
        sourceRef = serializedObject;

        GetProperties();
    }

    public override void OnInspectorGUI()
    {

        SetProperties();

        sourceRef.ApplyModifiedProperties();
    }

    public virtual void GetProperties()
    {
        //enemy
        attackTarget = sourceRef.FindProperty("attackTarget");
        //skins
        setData = sourceRef.FindProperty("setData");
        data = sourceRef.FindProperty("data");
    }

    public virtual void SetProperties()
    {
        EditorGUILayout.Space();
        //skins
        EditorGUILayout.PropertyField(setData);
        if (setData.boolValue)
            EditorGUILayout.PropertyField(data);
        //enemy
        EditorGUILayout.PropertyField(attackTarget);

    }

}
