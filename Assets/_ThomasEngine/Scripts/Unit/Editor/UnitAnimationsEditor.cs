﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.Animations;

[CustomEditor(typeof(UnitAnimations))]
public class UnitAnimationsEditor : Editor
{
    protected SerializedObject sourceRef;
    //skins
    protected SerializedProperty anim;

    private AnimatorState[] states;
    private string[] stateNames;

    private int pop;
    public virtual void OnEnable()
    {
        sourceRef = serializedObject;

        GetProperties();
    }

    public override void OnInspectorGUI()
    {
        GetAnimStateNames();
        SetProperties();

        sourceRef.ApplyModifiedProperties();
    }

    public virtual void GetProperties()
    {
        anim = sourceRef.FindProperty("anim");
    }

    public virtual void SetProperties()
    {
        EditorGUILayout.Space();
        //skins
        EditorGUILayout.PropertyField(anim);
        if (anim.objectReferenceValue)
            pop = EditorGUILayout.Popup(pop, stateNames);
        

    }

    void GetAnimStateNames()
    {
        var animObj = SerializedPropertyExtensions.GetValueFromComponent<Animator>(anim);
        if (animObj)
        {
            states = new AnimatorState[animObj.runtimeAnimatorController.animationClips.Length];
            states = EditorUtils.GetStateNames(animObj);
            if (states.Length > 0)
            {
                stateNames = new string[states.Length];
                for (int i = 0; i < states.Length; i++)
                {
                    stateNames[i] = states[i].name;
                }
            }
            
        }
    }

}
