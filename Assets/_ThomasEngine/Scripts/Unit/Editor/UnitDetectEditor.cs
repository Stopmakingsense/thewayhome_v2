﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor (typeof (UnitDetect))]
public class UnitDetectEditor : Editor
{
    protected UnitDetect source;
    protected SerializedObject sourceRef;

    protected SerializedProperty detectType;
    protected SerializedProperty detectPos;
    protected SerializedProperty detectSize;
    protected SerializedProperty detectRadius;
    protected SerializedProperty mask;
    protected SerializedProperty debugColor;

    public virtual void OnEnable ()
    {
        source = (UnitDetect)target;
        sourceRef = serializedObject;

        GetProperties ();
    }

    public override void OnInspectorGUI ()
    {
        SetProperties ();

        sourceRef.ApplyModifiedProperties ();
    }

    public virtual void GetProperties ()
    {
        detectType = sourceRef.FindProperty ("detectType");
        detectPos = sourceRef.FindProperty("detectPos");
        detectSize = sourceRef.FindProperty("detectSize");
        detectRadius = sourceRef.FindProperty("detectRadius");
        mask = sourceRef.FindProperty("mask");
        debugColor = sourceRef.FindProperty("debugColor");
    }

    public virtual void SetProperties ()
    {
        EditorGUILayout.Space();
        EditorGUILayout.PropertyField (detectType);
        EditorGUILayout.PropertyField (detectPos);
        if (detectType.enumValueIndex == 0)
            EditorGUILayout.PropertyField (detectRadius);
        else
            EditorGUILayout.PropertyField (detectSize);

        EditorGUILayout.PropertyField(mask);
        EditorGUILayout.PropertyField(debugColor);

    }

    public virtual void OnSceneGUI()
    {
        Handles.color = debugColor.colorValue;

        Vector2 pos = (Vector2)source.transform.position + detectPos.vector2Value;
        if (detectType.enumValueIndex == 0)
            Handles.DrawWireDisc(pos, Vector3.back, detectRadius.floatValue);
        else
            Handles.DrawWireCube(pos, detectSize.vector2Value);
    }

}