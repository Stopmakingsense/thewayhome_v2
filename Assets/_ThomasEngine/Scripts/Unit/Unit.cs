﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit : MonoBehaviour
{
    [SerializeField] protected bool setData;
    [SerializeField] protected UnitData data;
    protected UnitData curData;
    public UnitData curUnitData { get { return curData; } }
    [SerializeField] protected Transform attackTarget;
    public Transform AttackTarget { get { return attackTarget; } }

    protected int maxHp = 5;
    public int MaxHp { get { return maxHp; } set { maxHp = value; } }
    protected FloatWrapper curHp = new FloatWrapper(0);
    public FloatWrapper CurHP { get { return curHp; } }
    protected float curWeight;
    public float CurWeight { get { return curWeight; } }
    protected bool dead;
    public bool IsDead { get { return dead; } }
    protected UnitController controller;
    protected UIUnit ui;
    public UIUnit UI { get { return ui; } }

    public List<UnitBuff> curBuffs = new List<UnitBuff>();

    protected UnitAnimations anim;
    protected Rigidbody2D rb;

    public virtual void Awake()
    {
        GetComponents();
        if (setData)
            SetData(data);
    }

    public virtual void GetComponents()
    {
        controller = GetComponent<UnitController>();
        anim = GetComponent<UnitAnimations>();
        rb = GetComponent<Rigidbody2D>();
    }

    public virtual void SetData(UnitData _data)
    {
        curData = _data;
        //update unit stats
        //health
        maxHp = curData.maxHP;
        curHp.Value = maxHp;
        //weight
        curWeight = curData.weight;
        if (rb)
            rb.mass = curWeight;

        //update controller stats
        if (controller)
        {
            controller.BaseSpeed = curData.speed;
            controller.JumpPower = curData.jumpPower;
            controller.StartColSize = curData.skinSize;
            controller.StartColOffset = curData.skinSize / 2;
        }

        //buffs
        ActivateAllBuffs(false);
        curBuffs.Clear();
        curBuffs.AddRange(curData.buffs);
        ActivateAllBuffs(true);

        //spawn ui
        if (!ui && curData.UIToSpawn)
            ui = Instantiate(curData.UIToSpawn);

    }

    public virtual void AddBuff(UnitBuff _buffToAdd)
    {
        _buffToAdd.ActivateBuff(this, true);
        curBuffs.Add(_buffToAdd);
    }

    public virtual void RemoveBuff(UnitBuff _buffToRemove)
    {
        if (!curBuffs.Contains(_buffToRemove))
            return;

        _buffToRemove.ActivateBuff(this, false);
        curBuffs.Remove(_buffToRemove);
    }

    public virtual void ActivateAllBuffs(bool _activate)
    {
        foreach (var buff in curBuffs)
        {
            buff.ActivateBuff(this, _activate);
        }
    }

    public virtual void AddHp(float _amount)
    {
        //only add health if not at max
        if (curHp.Value < maxHp)
        {
            curHp.Value += _amount;
        }

    }

    public virtual void DamageHp(float _damage)
    {
        if (dead)
            return;

        curHp.Value -= _damage;
        curHp.Value = Mathf.Clamp(curHp.Value, 0, Mathf.Infinity);
        if (ui)
            ui.SetHPValue(curHp.Value);

        //anim
        if (anim)
            anim.PlayHurt();

        //kill player if health at 0
        if (curHp.Value <= 0)
        {
            curHp.Value = 0;
            Die();
        }

    }

    public virtual void DamageHp(float _damage, float _bounceForce, Vector2 _bounceDir)
    {
        if (dead)
            return;

        DamageHp(_damage);
        if (controller)
            controller.Bounce(_bounceDir, _bounceForce);

    }

    public virtual void Die(string _reason = default(string))
    {
        dead = true;

        ActivateAllBuffs(false);

        //anim
        if (anim)
            anim.PlayDead();

        if (data.destroyOnDeath)
        {
            Destroy(gameObject, data.delay);
        }
            
    }

}
