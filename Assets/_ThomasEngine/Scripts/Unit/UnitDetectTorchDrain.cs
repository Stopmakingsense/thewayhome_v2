﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ItemTorch))]
public class UnitDetectTorchDrain : UnitDetect
{
    [SerializeField] private bool lightDrain = true;
    [SerializeField] private bool hpDrain;
    private ItemTorch torch;

    private void Start()
    {
        torch = GetComponent<ItemTorch>();
    }
    public override void OnEnter()
    {
        base.OnEnter();
        if (lightDrain)
            torch.EnableLightDrain(false);
        if (hpDrain)
            torch.EnableHealthDrain(false);
    }

    public override void OnExit()
    {
        base.OnExit();
        if (lightDrain)
            torch.EnableLightDrain(true);
        if (hpDrain)
            torch.EnableHealthDrain(true);
    }

}
