﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class UnitDetect : MonoBehaviour
{
    public enum DetectType { Circle, Box }

    [SerializeField] protected DetectType detectType;
    [SerializeField] protected float detectRadius = 1;
    [SerializeField] protected Vector2 detectSize = Vector2.one;
    [SerializeField] protected Vector2 detectPos;
    [SerializeField] protected LayerMask mask;
    [SerializeField] protected Color debugColor = Color.cyan;

    protected Collider2D col;
    public bool IsDetected { get { return col; } }
    private bool lastDetect;

    public virtual void OnEnter()
    {
        //do stuff
    }

    public virtual void OnStay()
    {
        //do stuff
    }

    public virtual void OnExit()
    {
        //do stuff
    }

    public virtual void FixedUpdate()
    {
        DoDetection();
    }

    void DoDetection()
    {
        var pos = (Vector2)transform.TransformPoint(detectPos);
        if (detectType == DetectType.Box)
            col = Physics2D.OverlapBox(pos, detectSize, 0, mask);
        else
            col = Physics2D.OverlapCircle(pos, detectRadius, mask);

        if (lastDetect != IsDetected)
        {
            if (IsDetected)
                OnEnter();
            else
                OnExit();
        }
        else if (IsDetected)
            OnStay();

        lastDetect = IsDetected;
    }

}
