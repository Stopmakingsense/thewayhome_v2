﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitAnimations : MonoBehaviour
{
    public enum SyncType { SyncFields, StateSwitch}
    [SerializeField] private Animator anim;
    public Animator Animator { get { return anim; } set { anim = value; } }

    [SerializeField] private bool forceIdle;
    [SerializeField] private SyncType syncType;
    [SerializeField] private float crossfadeTime;

    //state fields
    [SerializeField] private string animIdle;
    [SerializeField] private string animHover;

    //param fields
    [SerializeField] private string animGrounded;

    //hybrid fields
    [SerializeField] private string animJump;
    [SerializeField] private string animHurt;
    [SerializeField] private string animDead;
    [SerializeField] private string animStunned;

    public virtual void PlayAnim(string _anim)
    {
        if (syncType == SyncType.SyncFields)
        {
            StartCoroutine(BoolSwitch(_anim));
        }
        else if (syncType == SyncType.StateSwitch)
        {
            anim.CrossFade(_anim, crossfadeTime);
        }
    }

    public virtual void PlayIdle()
    {
        PlayAnim(animIdle);
    }

    public virtual void PlayHurt()
    {
        PlayAnim(animHurt);
    }

    public virtual void PlayDead()
    {
        PlayAnim(animDead);
    }

    public virtual void PlayJump()
    {
        PlayAnim(animJump);
    }

    public virtual void PlayHover()
    {
        PlayAnim(animHover);
    }

    public virtual void PlayStunned()
    {
        PlayAnim(animStunned);
    }

    public virtual void SetGrounded(bool _grounded)
    {
        anim.SetBool(animGrounded, _grounded);
    }

    IEnumerator BoolSwitch(string _anim)
    {
        anim.SetBool(_anim, true);
        yield return new WaitForEndOfFrame();
        anim.SetBool(_anim, false);
    }       

    AnimatorControllerParameter FindParameter(string _param)
    {
        foreach (var param in anim.parameters)
        {
            if (param.name == _param)
                return param;
        }
        return null;
    }
}
