﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(EnemyController))]
[RequireComponent(typeof(UnitEquip))]
public class EnemyDetect : UnitDetect
{
    [SerializeField] private bool useItem;
    [SerializeField] private bool stopItemUseOnExit;

    private EnemyController enc;
    private UnitEquip equip;

    private void Start()
    {
        enc = GetComponent<EnemyController>();
        equip = GetComponent<UnitEquip>();
    }

    public override void OnEnter()
    {
        enc.SetAimTarget(col.gameObject.transform);
        equip.UseEquippedItem();
    }

    public override void OnExit()
    {
        if (stopItemUseOnExit)
            equip.StopUseEquippedItem();
    }
}
