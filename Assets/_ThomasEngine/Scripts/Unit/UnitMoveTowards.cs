﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Unit))]
public class UnitMoveTowards : UnitDetect
{

    public enum MoveTowardType { OnDetection, Transform, ExternalSource }

    //detection
    [SerializeField] protected MoveTowardType moveType;
    [SerializeField] protected GameObject moveToPoint;

    //movement
    [SerializeField] protected bool setSpeed;
    [SerializeField] protected float speed = 3;
    [SerializeField] protected float maxArrivalDistance = 2;
    [SerializeField] protected float maxChaseDistance = 10;
    [SerializeField] protected float maxHeightDifference = 1;
    [SerializeField] protected bool ignoreZTurning = true;
    [SerializeField] protected bool ignoreTargetYPos = true;

    //arrival options
    [SerializeField] protected bool activateArrivalEvents;
    [SerializeField] protected UnityEvent unityEvents;

    protected bool chasing;
    public bool IsChasing() { return chasing; }
    protected bool arrived;
    public bool HasArrived() { return arrived; }
    protected bool targetInRange;
    public bool IsTargetInRange() { return targetInRange; }
    protected bool targetTooHigh;
    protected float distance;
    protected Transform target;
    protected Unit unit;
    protected UnitController controller;

    public virtual void Start()
    {
        GetComponents();
        if (!setSpeed)
            speed = unit.curUnitData.speed;

        CheckDetectType();
    }

    public override void OnEnter()
    {
        MoveTowardsTarget(col.gameObject);
    }

    public override void FixedUpdate()
    {
        if (moveType != MoveTowardType.OnDetection)
            return;
        base.FixedUpdate();
    }

    public virtual void GetComponents()
    {
        unit = GetComponent<Unit>();
        controller = GetComponent<UnitController>();
    }

    void CheckDetectType()
    {
        if (moveType == MoveTowardType.Transform)
            MoveTowardsTarget(moveToPoint);
    }

    public virtual void MoveTowardsTarget(GameObject _target)
    {
        if (chasing)
            return;

        chasing = true;
        target = _target.transform;
        StopAllCoroutines();
        StartCoroutine(StartMoveTowardsTarget());
    }

    IEnumerator StartMoveTowardsTarget()
    {
        CheckTargetDistance();
        while (targetInRange && !arrived && !unit.IsDead)
        {
            CheckTargetDistance();
            CheckHeightDifference();   
                if (!targetTooHigh)
                {
                    MoveToTarget();
                    RotateTowardsTarget();
                }
            //wait one frame and return
            yield return new WaitForFixedUpdate();
        }
        if (!targetInRange)
            StopChasing();
        else if (arrived)
            DoArrivalEvents();
        
    }

    public virtual void CheckTargetDistance()
    {
        distance = Vector2.Distance(target.position, transform.position);
        targetInRange = distance < maxChaseDistance;
        arrived = distance < maxArrivalDistance;
    }

    void CheckHeightDifference()
    {
        //stop enemy from turning/moving if the target is directly above or below
        float vDiff = Mathf.Abs(target.position.y - transform.position.y);
        float hDiff = Mathf.Abs(target.position.x - transform.position.x);
        targetTooHigh = vDiff > maxHeightDifference && hDiff < maxArrivalDistance;
    }

    public virtual void StopChasing()
    {
        StopAllCoroutines();
        chasing = false;
    }

    public virtual void DoArrivalEvents()
    {
        if (!activateArrivalEvents)
            return;

        unityEvents.Invoke();
    }

    void MoveToTarget()
    {
        var moveSpeed = speed * controller.SpeedMultiplier * Time.deltaTime;
        if (ignoreTargetYPos)
            transform.Translate(transform.right * moveSpeed, Space.World);
        else
            transform.position = Vector2.MoveTowards(transform.position, target.position, moveSpeed * Time.deltaTime);
    }

    void RotateTowardsTarget()
    {
        if (ignoreZTurning)
            transform.right = new Vector3(target.position.x, transform.position.y) - transform.position;
        else
            transform.right = target.position - transform.position;
    }

    public void StopMoveTowardsTarget ()
    {
        StopAllCoroutines ();
    }
}