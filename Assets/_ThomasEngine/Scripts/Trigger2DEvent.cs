﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Internal;

[RequireComponent(typeof(Collider2D))]
public class Trigger2DEvent : MonoBehaviour
{
    [SerializeField] private bool buttonDownToTrigger;
    [SerializeField] private InputProperty button;

    [SerializeField] private float delayTriggerTime;

    [SerializeField] private bool useRepeatDelay;
    [SerializeField] private float repeatDelay = 0.1f;
    private float repeatTimer;

    public int mask = 0;
    public string[] maskOptions = new string[] { "Trigger2DEnter", "Trigger2DExit", "Trigger2DStay" };

    [SerializeField] private string triggerTag = "Untagged";

    //events
    [SerializeField] private bool useUnityEvents;
    [SerializeField] private UnityEvent trigger2DEnterEvents;
    [SerializeField] private UnityEvent trigger2DExitEvents;
    [SerializeField] private UnityEvent trigger2DStayEvents;

    //interacts
    [SerializeField] private InteractFX[] trigger2DEnterInteracts;
    [SerializeField] private InteractFX[] trigger2DExitInteracts;
    [SerializeField] private InteractFX[] trigger2DStayInteracts;

    private bool startTrigger;

    private void Start()
    {
        StartCoroutine(StartWait());
    }

    IEnumerator StartWait()
    {
        EnableColliders(false);
        yield return new WaitForSeconds(delayTriggerTime);
        EnableColliders(true);
        startTrigger = true;
    }

    void EnableColliders(bool _enable)
    {
        foreach (var col in GetComponents<Collider2D>())
        {
            col.enabled = _enable;
        }
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (!startTrigger)
            return;

        if (mask == 1 | mask == 3 | mask == 5 | mask == -1)
        {
            if (col.tag == triggerTag)
            {
                if (useUnityEvents)
                    trigger2DEnterEvents.Invoke();
                DoInteractFX(trigger2DEnterInteracts, col);
            }
        }
        else
            return;    
    }

    private void OnTriggerExit2D(Collider2D col)
    {
        if (!startTrigger)
            return;

        if (mask == 2 | mask == 3 | mask == 6 | mask == -1)
        {

            if (col.tag == triggerTag)
            {
                if (useUnityEvents)
                    trigger2DExitEvents.Invoke();
                DoInteractFX(trigger2DExitInteracts, col);
            }
        }
        else
            return;     
    }

    private void OnTriggerStay2D(Collider2D col)
    {
        if (!startTrigger)
            return;

        if (mask == 4 | mask == 5 | mask == 6 | mask == -1)
        {
            if (col.tag == triggerTag)
            {

                if (buttonDownToTrigger)
                {
                    if (!Input.GetButtonDown(button.stringValue))
                        return;
                }
                if (useRepeatDelay)
                {
                    repeatTimer += Time.deltaTime;
                    if (repeatTimer > repeatDelay)
                    {
                        if (useUnityEvents)
                            trigger2DStayEvents.Invoke();
                        DoInteractFX(trigger2DStayInteracts, col);
                        repeatTimer = 0;
                    }
                }
                else
                {
                    if (useUnityEvents)
                        trigger2DStayEvents.Invoke();
                    DoInteractFX(trigger2DStayInteracts, col);
                }
                
            }
        }
        else
            return;
    }

    void DoInteractFX(InteractFX[] _fx, Collider2D _col)
    {
        foreach (var fx in _fx)
        {
            fx.DoFX(this.gameObject, _col.gameObject);
        }
    }

}
