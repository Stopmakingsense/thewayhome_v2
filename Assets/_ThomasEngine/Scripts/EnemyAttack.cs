﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Enemy))]
[RequireComponent(typeof(EnemyController))]
[RequireComponent(typeof(UnitEquip))]
public class EnemyAttack : UnitDetect
{
    private UnitEquip equip;
    private EnemyController enc;

    private void Start()
    {
        enc = GetComponent<EnemyController>();
        equip = GetComponent<UnitEquip>();
    }

    public override void OnEnter()
    {
        base.OnEnter();
        var unit = col.GetComponent<Unit>();
        if (unit)
            enc.SetAimTarget(unit.AttackTarget);
        equip.UseEquippedItem();
    }

    public override void OnExit()
    {
        base.OnExit();
        equip.StopUseEquippedItem();
        enc.SetAimTarget(null);
    }
}