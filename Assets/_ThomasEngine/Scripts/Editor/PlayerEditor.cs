﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;

[CustomEditor(typeof(Player))]
public class PlayerEditor : UnitEditor
{

    //health
    private SerializedProperty maxLives;
    //physics ignore
    private SerializedProperty ignorePhysicsOnHit;
    private SerializedProperty ignoreLayerOne;
    private SerializedProperty ignoreLayerTwo;
    private SerializedProperty ignoreTime;
    private SerializedProperty stunTime;
    //material swaps
    private SerializedProperty changeMeshMaterialOnHit;
    private SerializedProperty materialToUse;


    public override void GetProperties()
    {
        base.GetProperties();
        maxLives = sourceRef.FindProperty("maxLives");

        //ignore physics
        ignorePhysicsOnHit = sourceRef.FindProperty("ignorePhysicsOnHit");
        ignoreLayerOne = sourceRef.FindProperty("ignoreLayerOne");
        ignoreLayerTwo = sourceRef.FindProperty("ignoreLayerTwo");
        ignoreTime = sourceRef.FindProperty("ignoreTime");
        stunTime = sourceRef.FindProperty("stunTime");
        //material change
        changeMeshMaterialOnHit = sourceRef.FindProperty("changeMeshMaterialOnHit");
        materialToUse = sourceRef.FindProperty("materialToUse");
    }

    public override void SetProperties()
    {

        base.SetProperties();
        //health
        EditorGUILayout.PropertyField(maxLives);

        //ignore physics
        EditorGUILayout.PropertyField(ignorePhysicsOnHit);
        if (ignorePhysicsOnHit.boolValue)
        {
            ignoreLayerOne.intValue = EditorGUILayout.LayerField("Ignore Layer One", ignoreLayerOne.intValue);
            ignoreLayerTwo.intValue = EditorGUILayout.LayerField("Ignore Layer Two", ignoreLayerTwo.intValue);
            EditorGUILayout.PropertyField(ignoreTime);
        }
        EditorGUILayout.PropertyField(stunTime);
        //material change
        EditorGUILayout.PropertyField(changeMeshMaterialOnHit);
        if(changeMeshMaterialOnHit.boolValue)
            EditorGUILayout.PropertyField(materialToUse);

    }

}
