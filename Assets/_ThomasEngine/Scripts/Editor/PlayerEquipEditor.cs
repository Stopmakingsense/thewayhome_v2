﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;

[CustomEditor(typeof(PlayerEquip))]
public class PlayerEquipEditor : UnitEquipEditor
{

    private SerializedProperty quickMenuButtons;
    private SerializedProperty seperateUseButtonsForItems;
    private SerializedProperty useButtons;
    private SerializedProperty useButton;
    private SerializedProperty equipButton;
    private SerializedProperty dropButton;


    public override void GetProperties()
    {
        base.GetProperties();
        quickMenuButtons = sourceRef.FindProperty("quickMenuButtons");
        seperateUseButtonsForItems = sourceRef.FindProperty("seperateUseButtonsForItems");
        useButtons = sourceRef.FindProperty("useButtons");
        useButton = sourceRef.FindProperty("useButton");
        equipButton = sourceRef.FindProperty("equipButton");
        dropButton = sourceRef.FindProperty("dropButton");
    }

    public override void SetProperties()
    {
        base.SetProperties();
        EditorGUILayout.PropertyField(seperateUseButtonsForItems);
        if (seperateUseButtonsForItems.boolValue)
        {
            EditorUtils.SetSerializedListSize(useButtons, itemDatas.arraySize);
            EditorGUILayout.PropertyField(useButtons, true);
        }
        else
        {
            if (setInventoryItems.boolValue)
                EditorUtils.SetSerializedListSize(quickMenuButtons, itemDatas.arraySize);
            else
                EditorUtils.SetSerializedListSize(quickMenuButtons, maxItems.intValue);
            EditorGUILayout.PropertyField(quickMenuButtons, true);

            EditorGUILayout.PropertyField(useButton);
            if (!autoEquipItems.boolValue)
                EditorGUILayout.PropertyField(equipButton);
            EditorGUILayout.PropertyField(dropButton);
        }


    }

    

}
