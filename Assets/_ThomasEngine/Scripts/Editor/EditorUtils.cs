﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEditor.Animations;
using System.Linq;

public class EditorUtils
{

    public static void DisplayAllChildrenPopup(string _fieldName, SerializedProperty _goProperty, SerializedProperty _indexProperty, SerializedProperty _stringProperty)
    {

        GameObject go = _goProperty.objectReferenceValue as GameObject;
        if (!go)
        {
            EditorGUILayout.LabelField(_fieldName, _goProperty.displayName + " is empty!");
            return;
        }
        else if (go.transform.childCount < 1)
        {
            EditorGUILayout.LabelField(_fieldName, _goProperty.displayName + " Must Have Children!");
            return;
        }

        //put all child names into array
        Transform[] childs = go.GetComponentsInChildren<Transform>();
        var childNames = new string[childs.Length];
        for (int i = 1; i < childs.Length; i++)
        {
            childNames[i] = childs[i].name;
        }

        //display popup
        _indexProperty.intValue = EditorGUILayout.Popup(_fieldName, _indexProperty.intValue, childNames);
        if (_indexProperty.intValue < childNames.Length)
            _stringProperty.stringValue = childNames[_indexProperty.intValue];
    }

    public static void DisplayAllInputAxisPopup(string _fieldName, SerializedProperty _indexProperty, SerializedProperty _stringProperty)
    {
        //put all input managers axis into an array
        var inputManager = AssetDatabase.LoadAllAssetsAtPath("ProjectSettings/InputManager.asset")[0];
        var obj = new SerializedObject(inputManager);
        var axisArray = obj.FindProperty("m_Axes");
        var inputAxisNames = new string[axisArray.arraySize];
        for (int i = 0; i < inputAxisNames.Length; i++)
        {
            inputAxisNames[i] = axisArray.GetArrayElementAtIndex(i).FindPropertyRelative("m_Name").stringValue;
        }

        //display popup
        _indexProperty.intValue = EditorGUILayout.Popup(_fieldName, _indexProperty.intValue, inputAxisNames);
        SerializedProperty elementHor = axisArray.GetArrayElementAtIndex(_indexProperty.intValue);
        _stringProperty.stringValue = elementHor.FindPropertyRelative("m_Name").stringValue;
    }

    public static void SpritePreviewField(SerializedProperty _sprite, float _width, float _height, bool _stretchFit)
    {
        EditorGUILayout.BeginHorizontal();
        if (_sprite.objectReferenceValue != null)
        {
            var sprite = (Sprite)_sprite.objectReferenceValue;
            if (sprite)
            {
                GUIStyle style = new GUIStyle();
                style.fixedHeight = _height;
                style.fixedWidth = _width;
                style.alignment = TextAnchor.MiddleCenter;
                style.stretchHeight = _stretchFit;
                style.stretchWidth = _stretchFit;
                GUILayout.Box(sprite.texture, style);
            }

        }
        EditorGUILayout.PropertyField(_sprite);
        EditorGUILayout.EndHorizontal();
    }

    public static void DrawHandleBox(SerializedProperty handleLeft, SerializedProperty handleRight)
    {

    }

    public static void DrawDetectZone(Transform _trans, SerializedProperty _detectZone)
    {
        var detectType = _detectZone.FindPropertyRelative("detectType");
        var offset = _detectZone.FindPropertyRelative("offset");
        var size = _detectZone.FindPropertyRelative("size");
        var useTransformZAngle = _detectZone.FindPropertyRelative("useTransformZAngle");
        var angle = _detectZone.FindPropertyRelative("angle");
        var radius = _detectZone.FindPropertyRelative("radius");
        var debugColor = _detectZone.FindPropertyRelative("debugColor");

        //set color
        Handles.color = debugColor.colorValue;
        //set position
        var pos = (Vector2)_trans.position + offset.vector2Value;

        //draw circle
        if (detectType.enumValueIndex == 0)
        {
            Handles.DrawWireDisc(pos, Vector3.back, radius.floatValue);
        }
        //draw box
        else if (detectType.enumValueIndex == 1)
        {
            if (useTransformZAngle.boolValue)
                angle.floatValue = _trans.eulerAngles.z;

            Matrix4x4 boxMatrix = Matrix4x4.TRS(pos, Quaternion.Euler(0, 0, angle.floatValue), Handles.matrix.lossyScale);
            using (new Handles.DrawingScope(boxMatrix))
            {
                Handles.DrawWireCube(Vector2.zero, size.vector2Value);
            }

        }

    }

    public static void PropertyFieldCustom(SerializedProperty _property, string _label, bool _includeChildren = false, Texture _image = null, string _toolTip = null)
    {
        GUIContent content = new GUIContent()
        {
            text = _label,
            image = _image,
            tooltip = _toolTip
        };

        EditorGUILayout.PropertyField(_property, content, _includeChildren);
    }

    public static void PrefabFieldWithComponent(SerializedProperty _gameobjectProperty, System.Type _componentType)
    {
        _gameobjectProperty.objectReferenceValue =
            EditorGUILayout.ObjectField(_gameobjectProperty.displayName, _gameobjectProperty.objectReferenceValue, typeof(GameObject), false);

        var prefab = _gameobjectProperty.objectReferenceValue;
        if (prefab)
        {
            var obj = prefab as GameObject;
            if (obj)
            {
                if (!obj.GetComponent(_componentType))
                {
                    Debug.Log(obj.name + " does not have component: " + _componentType.Name + ". " + _gameobjectProperty.displayName + 
                        " field requires a prefab with a " + _componentType.Name + " component.");
                    _gameobjectProperty.objectReferenceValue = null;
                }
            }
            
        }
    }

    public static void ScriptableObjectFieldType(SerializedProperty _scriptableProperty, System.Type _type)
    {
        _scriptableProperty.objectReferenceValue =
            EditorGUILayout.ObjectField(_scriptableProperty.displayName, _scriptableProperty.objectReferenceValue, typeof(ScriptableObject), false);

        var obj = _scriptableProperty.objectReferenceValue;
        if (obj)
        {
            var type = obj.GetType();
            if (type != _type)
            {
                    Debug.Log(obj.name + " is not of type: " + _type.Name + ". " + _scriptableProperty.displayName +
                        " field requires to be  " + _type.Name);
                    _scriptableProperty.objectReferenceValue = null;
            }

        }
    }

    public static AnimatorState[] GetStateNames(Animator _animator)
    {
        AnimatorController controller = _animator ? _animator.runtimeAnimatorController as AnimatorController : null;
        return controller == null ? null : controller.layers.SelectMany(l => l.stateMachine.states).Select(s => s.state).ToArray();
    }

    public static void SetSerializedListSize(SerializedProperty _targetList, int _size)
    {
        if (_targetList.arraySize == _size)
            return;

        while (_targetList.arraySize < _size)
        {
            _targetList.InsertArrayElementAtIndex(_targetList.arraySize);
        }
        while (_targetList.arraySize > _size)
        {
            _targetList.DeleteArrayElementAtIndex(_targetList.arraySize - 1);
        }
    }
}
