﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Trigger2DEvent))]
public class Trigger2DEventEditor : Editor
{
    private Trigger2DEvent source;
    private SerializedObject sourceRef;
    //masks
    private SerializedProperty mask;
    private SerializedProperty triggerTag;
    //button
    private SerializedProperty buttonDownToTrigger;
    private SerializedProperty button;
    //delays
    private SerializedProperty delayTriggerTime;
    private SerializedProperty useRepeatDelay;
    private SerializedProperty repeatDelay;
    //events
    private SerializedProperty useUnityEvents;
    private SerializedProperty trigger2DEnterEvents;
    private SerializedProperty trigger2DExitEvents;
    private SerializedProperty trigger2DStayEvents;
    //interacts
    private SerializedProperty trigger2DEnterInteracts;
    private SerializedProperty trigger2DExitInteracts;
    private SerializedProperty trigger2DStayInteracts;

    private void OnEnable()
    {
        source = (Trigger2DEvent)target;
        sourceRef = serializedObject;

        buttonDownToTrigger = sourceRef.FindProperty("buttonDownToTrigger");
        button = sourceRef.FindProperty("button");

        mask = sourceRef.FindProperty("mask");
        triggerTag = sourceRef.FindProperty("triggerTag");
        delayTriggerTime = sourceRef.FindProperty("delayTriggerTime");
        useRepeatDelay = sourceRef.FindProperty("useRepeatDelay");
        repeatDelay = sourceRef.FindProperty("repeatDelay");
        //events
        useUnityEvents = sourceRef.FindProperty("useUnityEvents");
        trigger2DEnterEvents = sourceRef.FindProperty("trigger2DEnterEvents");
        trigger2DExitEvents = sourceRef.FindProperty("trigger2DExitEvents");
        trigger2DStayEvents = sourceRef.FindProperty("trigger2DStayEvents");
        //interacts
        trigger2DEnterInteracts = sourceRef.FindProperty("trigger2DEnterInteracts");
        trigger2DExitInteracts = sourceRef.FindProperty("trigger2DExitInteracts");
        trigger2DStayInteracts = sourceRef.FindProperty("trigger2DStayInteracts");
    }

    public override void OnInspectorGUI()
    {
        triggerTag.stringValue = EditorGUILayout.TagField("Trigger2D Tag", triggerTag.stringValue);
        mask.intValue = EditorGUILayout.MaskField("Trigger2D Types", mask.intValue, source.maskOptions);

        EditorGUILayout.PropertyField(delayTriggerTime);
        int i = mask.intValue;

        if (i == 1 | i == 3 | i == 5 | i == -1)
        {
            EditorGUILayout.PropertyField(useUnityEvents);
            
            if (useUnityEvents.boolValue)
                EditorGUILayout.PropertyField(trigger2DEnterEvents);

                EditorGUILayout.PropertyField(trigger2DEnterInteracts, true);
        }


        if (i == 2 | i == 3 | i == 6 | i == -1)
        {
            EditorGUILayout.PropertyField(useUnityEvents);
            if (useUnityEvents.boolValue)
                EditorGUILayout.PropertyField(trigger2DExitEvents);

            EditorGUILayout.PropertyField(trigger2DExitInteracts, true);

        }


        if (i == 4 | i == 6 | i == 5 | i == -1)
        {
            EditorGUILayout.PropertyField(buttonDownToTrigger);
            if (buttonDownToTrigger.boolValue)
            {
                EditorGUILayout.PropertyField(button);
            }
            EditorGUILayout.PropertyField(useRepeatDelay);
            if (useRepeatDelay.boolValue)
            {
                EditorGUILayout.PropertyField(repeatDelay);
            }
            EditorGUILayout.PropertyField(useUnityEvents);
            if (useUnityEvents.boolValue)
                EditorGUILayout.PropertyField(trigger2DStayEvents);

            EditorGUILayout.PropertyField(trigger2DStayInteracts, true);

        }


        sourceRef.ApplyModifiedProperties();
    }
	
}
