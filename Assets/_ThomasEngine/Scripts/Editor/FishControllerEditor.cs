﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(FishController))]
public class FishControllerEditor : PlayerControllerEditor
{
    private FishController fSource;
    //Movement Properties
    private SerializedProperty fallSpeed;
    private SerializedProperty inflatedSpeed;
    private SerializedProperty inflatedColliderSize;
    private SerializedProperty animProp;
    private SerializedProperty holdToInflate;
    private SerializedProperty inflateTime;
    private SerializedProperty useAddedForce;
    private SerializedProperty addedForcePower;
    private SerializedProperty consistentRiseForce;

    public override void OnEnable()
    {
        base.OnEnable();
        fSource = (FishController)source;
    }

    public override void GetProperties()
    {
        base.GetProperties();
        GetFishProperties();
    }

    public override void SetProperties()
    {
        DisplayFishProperties();
    }

    void GetFishProperties()
    {
        fallSpeed = sourceRef.FindProperty("fallSpeed");
        inflatedSpeed = sourceRef.FindProperty("inflatedSpeed");
        inflatedColliderSize = sourceRef.FindProperty("inflatedColliderSize");
        animProp = sourceRef.FindProperty("animProp");
        holdToInflate = sourceRef.FindProperty("holdToInflate");
        inflateTime = sourceRef.FindProperty("inflateTime");
        useAddedForce = sourceRef.FindProperty("useAddedForce");
        addedForcePower = sourceRef.FindProperty("addedForcePower");
        consistentRiseForce = sourceRef.FindProperty("consistentRiseForce");
    }

    void DisplayFishProperties()
    {
        EditorUtils.PropertyFieldCustom(jumpButton, "Inflate Button");
        EditorGUILayout.PropertyField(fallSpeed);
        EditorGUILayout.PropertyField(inflatedSpeed);
        EditorGUILayout.PropertyField(inflatedColliderSize);
        EditorGUILayout.LabelField("Anim Prop controls crossfade time for both animation and inflating");
        EditorGUILayout.PropertyField(animProp, true);
        EditorGUILayout.PropertyField(holdToInflate);
        if (!holdToInflate.boolValue)
            EditorGUILayout.PropertyField(inflateTime);
        EditorGUILayout.PropertyField(useAddedForce);
        if (useAddedForce.boolValue)
        {
            EditorGUILayout.PropertyField(addedForcePower);
            EditorGUILayout.PropertyField(consistentRiseForce);
        }

    }

    

}
