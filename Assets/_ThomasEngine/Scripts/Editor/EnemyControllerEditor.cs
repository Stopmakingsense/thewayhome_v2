﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;
using UnityEditor.SceneManagement;

[CustomEditor(typeof(EnemyController))]
public class EnemyControllerEditor : UnitControllerEditor
{

    private EnemyController eSource;
    //movement
    private SerializedProperty movementType;
    private SerializedProperty speed;
    //boundaries
    private SerializedProperty useBoundaries;
    private SerializedProperty resetBoundariesOnEnable;
    private SerializedProperty leftOffset;
    private SerializedProperty rightOffset;
    //collision hits
    private SerializedProperty flipOnCollision;
    //bounce
    private SerializedProperty bounce;
    private SerializedProperty bouncePower;
    private SerializedProperty bounceXAddedForce;
    private SerializedProperty bounceDelay;

    private Vector2 handleLeft;
    private Vector2 handleRight;

    private bool animFoldout;

    public override void OnEnable()
    {
        base.OnEnable();
        eSource = (EnemyController)source;
    }

    public override void GetProperties()
    {
        base.GetProperties();

        faceRightAtStart = sourceRef.FindProperty("faceRightAtStart");
        //movement
        movementType = sourceRef.FindProperty("movementType");
        useBoundaries = sourceRef.FindProperty("useBoundaries");
        resetBoundariesOnEnable = sourceRef.FindProperty("resetBoundariesOnEnable");
        leftOffset = sourceRef.FindProperty("leftOffset");
        rightOffset = sourceRef.FindProperty("rightOffset");
        //collision hits
        flipOnCollision = sourceRef.FindProperty("flipOnCollision");
        //bounce
        bounce = sourceRef.FindProperty("bounce");
        bouncePower = sourceRef.FindProperty("bouncePower");
        bounceXAddedForce = sourceRef.FindProperty("bounceXAddedForce");
        bounceDelay = sourceRef.FindProperty("bounceDelay");

    }

    public override void SetProperties()
    {
        DisplayMovement();
        DisplayAiming();
        DisplayBounce();
        if (movementType.enumValueIndex == 1)
        {
            DisplayBoundaries();
            DisplayGroundDetection();
            DisplaySideDetection();
        }

    }

    void DisplayBoundaries()
    {
        EditorGUILayout.Space();
        EditorGUILayout.PropertyField(useBoundaries);
        if (useBoundaries.boolValue)
        {
            EditorGUILayout.PropertyField(resetBoundariesOnEnable);
            EditorGUILayout.PropertyField(leftOffset);
            EditorGUILayout.PropertyField(rightOffset);
        }
    }

    void DisplayBounce()
    {
        EditorGUILayout.Space();
        EditorGUILayout.PropertyField(bounce);
        if (bounce.boolValue)
        {
            EditorGUILayout.PropertyField(bouncePower);
            EditorGUILayout.PropertyField(bounceDelay);
            if (movementType.enumValueIndex == 0)
            {
                EditorGUILayout.PropertyField(bounceXAddedForce);
            }

        }
    }

    public override void DisplayMovement()
    {
        base.DisplayMovement();
        EditorGUILayout.PropertyField(movementType);
    }

    public override void DisplaySideDetection()
    {
        EditorGUILayout.PropertyField(flipOnCollision);
        if (!flipOnCollision.boolValue)
            return;

        base.DisplaySideDetection();
    }

    public override void OnSceneGUI()
    {
        base.OnSceneGUI();
        if (movementType.enumValueIndex == 0)
            return;

        if (useBoundaries.boolValue)
            DrawPositionHandles();

    }

    public override void DrawSideGizmos()
    {
        if (!flipOnCollision.boolValue)
            return;
        base.DrawSideGizmos();
    }

    void DrawPositionHandles()
    {

        InitializeHandlePositions();

        EditorGUI.BeginChangeCheck();


        if (!Application.isPlaying)
        {
            eSource.leftBoundary = (Vector2)source.transform.TransformPoint(leftOffset.vector2Value);
            eSource.rightBoundary = (Vector2)source.transform.TransformPoint(rightOffset.vector2Value);

            if (handleLeft != eSource.leftBoundary)
            {
                handleLeft = Handles.PositionHandle(eSource.leftBoundary, Quaternion.identity);
            }
            else
                handleLeft = Handles.PositionHandle(handleLeft, Quaternion.identity);

            if (handleRight != eSource.rightBoundary)
            {
                handleRight = Handles.PositionHandle(eSource.rightBoundary, Quaternion.identity);
            }
            else
                handleRight = Handles.PositionHandle(handleRight, Quaternion.identity);

            if (EditorGUI.EndChangeCheck())//update script values after dragging
            {
                Undo.RecordObject(source, "Modified " + source + " properties.");
                if (handleLeft.x > source.transform.position.x)
                    handleLeft.x = source.transform.position.x - 1;
                if (handleRight.x < source.transform.position.x)
                    handleRight.x = source.transform.position.x + 1;

                eSource.leftOffset = source.transform.InverseTransformPoint(handleLeft);
                eSource.rightOffset = source.transform.InverseTransformPoint(handleRight);
            }
        }

        //draw icons
        DrawSideLines(eSource.leftBoundary, "Left Pos");
        DrawSideLines(eSource.rightBoundary, "Right Pos");

    }

    void InitializeHandlePositions()
    {
        //set Initial positions for other points so they don't all start at zero zero
        if (leftOffset.vector2Value == Vector2.zero)
        {
            eSource.leftOffset = Vector2.left * 3;
        }
        if (rightOffset.vector2Value == Vector2.zero)
        {
            eSource.rightOffset = Vector2.right * 3;
        }
    }

    void DrawSideLines(Vector2 _pos, string _labelName)
    {
        Handles.Label(_pos, _labelName);
        Handles.DrawSolidDisc(_pos, Vector3.back, 0.3f);
        Handles.DrawDottedLine(new Vector2(_pos.x, _pos.y - 5), new Vector2(_pos.x, _pos.y + 5), 5);
    }

}
