﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor (typeof (EnemyMoveTowards))]
public class EnemyMoveTowardsEditor : UnitMoveTowardsEditor
{
    private SerializedProperty useItem;

    public override void GetProperties ()
    {
        base.GetProperties();
        useItem = sourceRef.FindProperty ("useItem");
    }

    public override void SetProperties ()
    {
        base.SetProperties();
        EditorGUILayout.PropertyField (useItem);
    }

}