﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;

[CustomEditor(typeof(UnitEquip))]
public class UnitEquipEditor : Editor
{

    protected SerializedObject sourceRef;
    protected SerializedProperty autoEquipItems;
    protected SerializedProperty maxItems;
    protected SerializedProperty setInventoryItems;
    protected SerializedProperty setAllItemsActive;
    protected SerializedProperty itemDatas;
    protected SerializedProperty dropThrowPower;

    public virtual void OnEnable()
    {
        sourceRef = serializedObject;

        GetProperties();
    }

    public override void OnInspectorGUI()
    {

        SetProperties();

        sourceRef.ApplyModifiedProperties();
    }

    public virtual void GetProperties()
    {
        autoEquipItems = sourceRef.FindProperty("autoEquipItems");
        maxItems = sourceRef.FindProperty("maxItems");
        setInventoryItems = sourceRef.FindProperty("setInventoryItems");
        setAllItemsActive = sourceRef.FindProperty("setAllItemsActive");
        itemDatas = sourceRef.FindProperty("itemDatas");
        dropThrowPower = sourceRef.FindProperty("dropThrowPower");

    }

    public virtual void SetProperties()
    {
        EditorGUILayout.Space();
        EditorGUILayout.PropertyField(autoEquipItems);
        EditorGUILayout.PropertyField(setInventoryItems);
        EditorGUILayout.PropertyField(setAllItemsActive);
        if (setInventoryItems.boolValue)
            EditorGUILayout.PropertyField(itemDatas, true);
        else
            EditorGUILayout.PropertyField(maxItems);
        EditorGUILayout.PropertyField(dropThrowPower);


    }

}
