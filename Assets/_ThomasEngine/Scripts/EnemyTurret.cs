﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyTurret : MonoBehaviour
{

    [SerializeField]
    private GameObject projectile;
    [SerializeField]
    private bool lookAtPlayer = true;
    [SerializeField]
    private bool flipLook;
    [Header("must have an empty parent if you want to flip")]
    [SerializeField]
    private Transform flipParent;
    [SerializeField]
    private float rotateSensitivity;
    [SerializeField]
    private float rotLimit = 360;
    [SerializeField]
    private float shootDelay = 1;
    [SerializeField]
    private float projectileSpeed = 3;
    [SerializeField]
    private int damage = 1;
    [SerializeField]
    private Transform nozzle;

    private Transform target;
    private float timer;
    private float animSwitch = 0.3f;

    private bool attack;

    void Start()
    {

    }

    void OnTriggerStay2D(Collider2D _col)
    {
        if (_col.tag == "Player")
        {
            target = _col.GetComponent<Player>().AttackTarget;
            if (lookAtPlayer)
                LookAtPlayer(target);

            ShootProjectile();
        }
       
    }

    void OnTriggerExit2D(Collider2D _col)
    {
        if (_col.tag == "Player")
        {
            //timer = 0;
            //anim.powerUp = false;
            //anim.attack = false;
        }
    }

    void LookAtPlayer(Transform _target)
    {
        Vector3 diff = _target.position - transform.position ;
        //look at player only rotating on Z Axis
        if (flipLook)
        {
            if (_target.position.x < flipParent.position.x)
                diff = transform.position - _target.position;
            else
                diff = _target.position - transform.position;
        }
        
        diff.Normalize();
        float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
        rot_z = Mathf.Clamp(rot_z, -rotLimit, rotLimit);

        if (flipLook)
        {
            if (_target.position.x < flipParent.position.x)
            {
                float turnLerp = Mathf.Lerp(flipParent.localEulerAngles.y, 180, Time.deltaTime * rotateSensitivity);
                flipParent.localEulerAngles = new Vector3(0, turnLerp, 0);
                rot_z = -rot_z;
            }
            else
            {
                float turnLerp = Mathf.Lerp(flipParent.localEulerAngles.y, 0, Time.deltaTime * rotateSensitivity);
                flipParent.localEulerAngles = new Vector3(0, turnLerp, 0);
            }
        }

        Quaternion newRot = Quaternion.Euler(0f, 0f, rot_z);

        transform.localRotation = Quaternion.Lerp(transform.localRotation, newRot, Time.deltaTime * rotateSensitivity);
    }

    void ShootProjectile()
    {
        timer += Time.deltaTime;

        if (timer > animSwitch)
        {
            //anim.attack = false;
            //anim.powerUp = true;
        }
            

        if (timer > shootDelay)
        {
            SpawnProjectile();
            timer = 0;
        }
    }

    void SpawnProjectile()
    {
        //spawn prefab and store in a variable
        var spawn = Instantiate(projectile, nozzle.position, nozzle.rotation);

        //get projectile component
        Projectile pj = spawn.GetComponent<Projectile>();

        //set speed and damage on projectile prefab
        //pj.ShootProjectile(projectileSpeed, damage, -1, target);

        //anim.attack = true;
        //anim.powerUp = false;
    }
}
