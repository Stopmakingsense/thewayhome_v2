﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BoolWrapper
{
    public bool Value { get; set; }
    public BoolWrapper(bool value) { this.Value = value; }
}

[System.Serializable]
public class FloatWrapper
{
    public float Value { get; set; }
    public FloatWrapper(float value) { this.Value = value; }
}

[System.Serializable]
public class InputProperty
{
    public string stringValue;
    public int indexValue;
}

[System.Serializable]
public class TagProperty
{
    public string stringValue;
}

[System.Serializable]
public class LayerProperty
{
    public int indexValue;
}

[System.Serializable]
public class ChildName
{
    public GameObject parentPrefab;
    public string stringValue;
    public int indexValue;
}

[System.Serializable]
public class AnimatorStateProperty
{
    public string stateToPlay;
    public float crossfadeTime;
    public string exitState;
}



